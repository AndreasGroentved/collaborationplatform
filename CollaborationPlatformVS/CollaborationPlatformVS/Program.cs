﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using CollaborationPlatformVS.Code.Domain;
using CollaborationPlatformVS.Code.Util;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;


namespace CollaborationPlatformVS
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                    .UseKestrel()
                    .UseContentRoot(Directory.GetCurrentDirectory())
                    .UseStartup<Startup>()
                    .Build();

            host.Run();
        }
    }
}