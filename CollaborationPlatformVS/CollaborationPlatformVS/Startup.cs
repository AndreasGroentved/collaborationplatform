﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using System.Threading;
using CollaborationPlatformVS.Code.Connection;
using CollaborationPlatformVS.Code.Data;
using CollaborationPlatformVS.Code.Domain;
using CollaborationPlatformVS.Code.Domain.Managers;
using CollaborationPlatformVS.Code.Security;

namespace CollaborationPlatformVS
{
    public class Startup
    {
        public void Configure(IApplicationBuilder app, IServiceProvider serviceProvider)
        {
            Debug.WriteLine(Thread.CurrentThread.Name);
            app.UseWebSockets();
            app.Use(async (context, next) =>
            {
                Debug.WriteLine(Thread.CurrentThread.Name);
                await next();
                if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });

            app.MapWebSocketManager("/ws", serviceProvider.GetService<WebSocketCommunication>());
            app.UseStaticFiles();
        }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<Rsa>();
            services.AddSingleton<AES>();
            services.AddSingleton<IEncrypt, Encryption>();
            services.AddSingleton<IDataManager, DataManager>();
            services.AddSingleton<IUserManager, UserManager>();
            services.AddSingleton<IAttributeHandler, AttributeHandler>();
            services.AddSingleton<IUserSurveyHandler, UserSurveyHandler>();
            services.AddSingleton<ICollaborate, CollaborationPlatform>();

            services.AddWebSocketManager();
        }
    }
}