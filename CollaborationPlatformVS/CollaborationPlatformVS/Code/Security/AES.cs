﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CollaborationPlatformVS.Code.Security
{
    public class AES
    {
        private readonly byte[] _ivBytes;

        public AES()
        {
            _ivBytes = Encoding.UTF8.GetBytes("8080808080808080");
        }

        public string Encrypt(string toEncrypt, string passwordHash)
        {
            var keybytes = Convert.FromBase64String(passwordHash);
            return Convert.ToBase64String(Enc(toEncrypt, keybytes, _ivBytes));
        }

        public string Decrypt(string toDecrypt, string passwordHash)
        {
            var keybytes = Convert.FromBase64String(passwordHash);
            var encrypted = Convert.FromBase64String(toDecrypt);
            return Dec(encrypted, keybytes, _ivBytes);
        }


        public string DecryptBytes(byte[] toDecrypt, string passwordHash)
        {
            var keybytes = Convert.FromBase64String(passwordHash);
            var encrypted = toDecrypt;
            return Dec(encrypted, keybytes, _ivBytes);
        }

        private string Dec(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Declare the string used to hold  
            // the decrypted text.  
            string plaintext;

            // Create an RijndaelManaged object  
            // with the specified key and IV.  
            using (var rijAlg = Aes.Create())
            {
                //Settings  
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.  
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                try
                {
                    // Create the streams used for decryption.  
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream  
                                // and place them in a string.  
                                plaintext = srDecrypt.ReadToEnd();
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    plaintext = "keyError";
                    Debug.WriteLine("Exception: " + e.Message);
                }
            }

            return plaintext;
        }

        private byte[] Enc(string plainText, byte[] key, byte[] iv)
        {
            // Check arguments.  
            if (plainText == null || plainText.Length <= 0) throw new ArgumentNullException(nameof(plainText));
            if (key == null || key.Length <= 0) throw new ArgumentNullException(nameof(key));
            if (iv == null || iv.Length <= 0) throw new ArgumentNullException(nameof(key));

            byte[] encrypted;
            // Create a RijndaelManaged object  
            // with the specified key and IV.  
            using (var rijAlg = Aes.Create())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.  
                var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption.  
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.  
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            // Return the encrypted bytes from the memory stream.  
            return encrypted;
        }
    }
}