﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using RSAParameters = System.Security.Cryptography.RSAParameters;

namespace CollaborationPlatformVS.Code.Security
{
    public class Rsa
    {
        private readonly Encoding _byteConverter;
        private readonly RSAParameters _rsaParameters;
        private readonly RSA _rsa;

        public Rsa()
        {
            _rsa = RSA.Create();
            _byteConverter = new UTF8Encoding();
            _rsaParameters = new RSAParameters
            {
                Exponent = StringToByteArray("010001"),
                Modulus = StringToByteArray(
                    "bc281cfe8139c6617cdeb64335773a6d7a08fc2a4dab5d04a2f5ed27f2d534a5c6bf960f3ce1bb0e98f7fad610b48a712e4d61efce001167c4358cc75682f03810f4756df3d417694b12b53b3f480e23ad345f3d188788abe89e2b8225778ac2732cb4ce11ee95f42db114575ff983782fe1111fe516cf2876d6474f3fb876db"),
                D = StringToByteArray(
                    "77c8a9bcb4940d615ed781fd037aa5edb0352c23da526fe355c36e7972349d51b4579fb68fb1bd1e2bf022f648a847428a91a4f9bf91778331fc4d65520405dcf3eaaa0273c6a3d9207739372f1d6c2a5ee688112a0d4e7341ba73730c550fce1e6b9333b29e7a27a0d55703836a6f42cbc33ee1e4a3124375984481b90e44b1"),
                P = StringToByteArray(
                    "00e51997196d567981e0ea746aad99c8e06a67be57c38c913c6c53a5ff578271d720f7f6b8bd9171c1e48c2622ef2d664f8f178a5746a07a7424f305b94525a9e5"),
                DP = StringToByteArray(
                    "00a7cd6090ed36c374cfcaaa4e60ad1d26d288fe2fc639353fae906e2b626b56fa9d5941cafa7e8775def29014525f4c6ac582167fea692c87d9706f26ce70d1c9"),
                InverseQ = StringToByteArray(
                    "00ae96cf1346786e9eb91503a24d71b9520573dd5c29b96e2e95f486d6a15c25b90d570c0df7ebae501b2d0e4367d5b8352208ea476810da2d02e92e62472f0cfa"),
                DQ = StringToByteArray(
                    "38b5a8adff1cbed6e46cfb187729d359b6bc395a59979486748810fd0456427178ce933d5da339091e5679ddfcacbca0e8608431e9370a67f03dcaf4692c9ca7"),
                Q = StringToByteArray(
                    "00d23fd407019e462ccc059cb3e67c9c840c8a6465f04ce6d0ef1b60f1c11bb72e1d57666e5659cc7a4719e953e7128333dffc7982cee7add7bfc851b5ff9491bf")
            };
        }

        public string Decrypt(string encodedText)
        {
            byte[] byteText = StringToByteArray(encodedText);
            try
            {
                return Encoding.UTF8.GetString(RsaDecrypt(byteText, _rsaParameters));
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return "";
            }
        }

        public byte[] Encrypt(string toEncrypt) //TODO test hvis usecase findes...
        {
            byte[] dataToEncrypt = _byteConverter.GetBytes(toEncrypt);
            return RsaEncrypt(dataToEncrypt, _rsaParameters);
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                    .Where(x => x % 2 == 0).Select(x => Convert.ToByte(hex.Substring(x, 2), 16)).ToArray();
        }

        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba) hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        private byte[] RsaEncrypt(byte[] dataToEncrypt, RSAParameters rsaKeyInfo)
        {
            try
            {
                byte[] encryptedData;
                using (_rsa)
                {
                    _rsa.ImportParameters(rsaKeyInfo);
                    encryptedData = _rsa.Encrypt(dataToEncrypt, RSAEncryptionPadding.Pkcs1);
                }
                return encryptedData;
            }
            catch (CryptographicException e)
            {
                Debug.WriteLine(e.Message);
                return null;
            }
        }

        private byte[] RsaDecrypt(byte[] dataToDecrypt, RSAParameters rsaKeyInfo)
        {
            try
            {
                byte[] decryptedData;
                using (_rsa)
                {
                    _rsa.ImportParameters(rsaKeyInfo);

                    decryptedData = _rsa.Decrypt(dataToDecrypt, RSAEncryptionPadding.Pkcs1);
                }
                return decryptedData;
            }
            catch (CryptographicException e)
            {
                Debug.WriteLine(e.ToString());
                return null;
            }
        }
    }
}