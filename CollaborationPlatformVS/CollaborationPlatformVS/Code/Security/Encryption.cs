﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollaborationPlatformVS.Code.Security
{
    class Encryption : IEncrypt
    {
        private readonly Rsa _rsa;
        private readonly AES _aes;

        public Encryption(Rsa rsa, AES aes)
        {
            _rsa = rsa;
            _aes = aes;
        }

        public string RsaDecrypt(string encodedText) => _rsa.Decrypt(encodedText);

        public byte[] Encrypt(string toEncrypt) => _rsa.Encrypt(toEncrypt);

        public string Encrypt(string toEncrypt, string passwordHash) => _aes.Encrypt(toEncrypt, passwordHash);

        public string Decrypt(string toDecrypt, string passwordHash) => _aes.Decrypt(toDecrypt, passwordHash);

        public string DecryptBytes(byte[] toDecrypt, string passwordHash) => _aes.DecryptBytes(toDecrypt, passwordHash);
    }
}