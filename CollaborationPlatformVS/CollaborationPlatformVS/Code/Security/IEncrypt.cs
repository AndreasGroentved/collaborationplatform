﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CollaborationPlatformVS.Code.Security
{
    public interface IEncrypt
    {
        string RsaDecrypt(string encodedText);
        byte[] Encrypt(string toEncrypt);
        string Encrypt(string toEncrypt, string passwordHash);
        string Decrypt(string toDecrypt, string passwordHash);
        string DecryptBytes(byte[] toDecrypt, string passwordHash);
    }
}
