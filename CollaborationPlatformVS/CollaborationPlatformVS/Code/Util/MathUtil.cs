﻿using System;

namespace CollaborationPlatformVS.Code.Util
{
    public class MathUtil
    {

        public static bool WithInRange(int a, int b, double val) => Math.Min(a, b) <= val && val <= Math.Max(a, b);


    }
}
