﻿using System.Collections.Generic;
using CollaborationPlatformVS.Code.Domain.ClientServerDataObjects;

namespace CollaborationPlatformVS.Code.Util
{
    public class AttributComparer : IEqualityComparer<Attribute>
    {
        public bool Equals(Attribute x, Attribute y)
        {
            if (ReferenceEquals(x, y)) return true;
            return x != null && y != null && x.Name.Equals(y.Name);
        }

        public int GetHashCode(Attribute obj)
        {
            int hashProductName = obj.Name?.GetHashCode() ?? 0;
            int hashProductCode = obj.Value.GetHashCode();
            return hashProductName ^ hashProductCode;
        }
    }
}