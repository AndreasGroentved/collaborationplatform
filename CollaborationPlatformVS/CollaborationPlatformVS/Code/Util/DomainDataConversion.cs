﻿using CollaborationPlatformVS.Code.Data;
using CollaborationPlatformVS.Code.Domain.ClientServerDataObjects;

namespace CollaborationPlatformVS.Code.Util
{
    public class DomainDataConversion
    {
        private DomainDataConversion()
        {
        }

        //TODO nem test
        public static User ToUser(UserDTO userDto) => new User(userDto.Attributes, userDto.Email, userDto.Name);

        public static UserDTO ToUserDto(User user) => new UserDTO(user.Attributes, user.Email, user.Name);
    }
}