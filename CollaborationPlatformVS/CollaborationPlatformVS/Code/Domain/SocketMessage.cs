﻿namespace CollaborationPlatformVS.Code.Domain
{
    public class SocketMessage
    {
        public string Socketid;
        public string Message;

        public SocketMessage(string socketid, string message)
        {
            Socketid = socketid;
            Message = message;
        }

        public SocketMessage()
        {

        }
    }
}
