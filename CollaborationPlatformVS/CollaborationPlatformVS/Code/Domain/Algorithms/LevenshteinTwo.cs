﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CollaborationPlatformVS.Code.Util;
using static System.Int32;

namespace CollaborationPlatformVS.Code.Domain.Algorithms
{
    public static class LevenshteinTwo
    {
        public static int Distance(this string source, string target, int maxDistance = 2) => Dis(source.ToLower(), target.ToLower(), maxDistance);

        private static int Dis(string a, string b, int maxDistance)
        {
            if (a.Length == 0) return b.Length;
            if (b.Length == 0) return a.Length;
            if (Math.Abs(b.Length - a.Length) > 2) return 3;
            int n = a.Length >= b.Length ? a.Length + 1 : b.Length + 1;
            if (maxDistance > n - 1) maxDistance = n - 1;
            char[] w1 = a.ToCharArray(), w2 = b.ToCharArray();
            int[,] distance = new int[a.Length + 1, b.Length + 1];
            FillBorder(distance);
            int w = w1.Length + 1, h = w2.Length + 1;
            XY diag = new XY();
            int min = MaxValue;
            for (int dN = 1; dN < n; dN++)
            {
                GetTwoDiagonals(diag, dN, n, maxDistance);
                min = GetMin(diag, w, h, distance, w1, w2);
                if (min > maxDistance && min != MaxValue /*uneven length of words*/) return maxDistance + 1;
            }
            return min == MaxValue ? distance[w - 1, h - 1] : min;
        }

        private static void GetTwoDiagonals(XY diagList, int dN, int n, int distance)
        {
            diagList.Clear();
            diagList.Add(dN, dN);
            for (int i = 1; i < 1 + distance; i++)
            {
                diagList.Add(dN + i, dN - i);
                diagList.Add(dN - i, dN + i);
                if (dN >= n - 1) continue; /*Second diagonal, not needed if we are at last position  */
                diagList.Add(dN + i, dN + 1 - i);
                diagList.Add(dN + 1 - i, dN + i);
            }
        }

        private static int GetMin(XY diag, int w, int h, int[,] dis, char[] a, char[] b)
        {
            int min = MaxValue;
            for (int i = 0; i < diag.Size; i++)
            {
                if (!IsValid(diag.Get(i), w, h, dis)) continue;
                Point p = diag.Get(i);
                int val = CalculateVal(p, dis, a, b, w, h);
                min = Math.Min(val, min);
                dis[p.X, p.Y] = val;
            }
            return min;
        }

        private static bool IsValid(Point p, int w, int h, int[,] distance) => !IsOutOfBound(p.X, p.Y, w, h) && distance[p.X, p.Y] == 0;

        private static bool IsOutOfBound(int x, int y, int width, int height) => x > width - 1 || y < 0 || x < 0 || y > height - 1;

        private static void FillBorder(int[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++) arr[i, 0] = i;
            for (int i = 0; i < arr.GetLength(1); i++) arr[0, i] = i;
        }

        private static int CalculateVal(Point p, int[,] dis, char[] a, char[] b, int w, int h)
        {
            if (IsOutOfBound(p.X, p.Y, w, h)) return MaxValue;
            int intA = dis[p.X - 1, p.Y - 1] + ((a[p.X - 1].Equals(b[p.Y - 1]) ? 0 : 1));
            int intB = dis[p.X - 1, p.Y] + 1;
            int intC = dis[p.X, p.Y - 1] + 1;
            return Math.Min(intA, Math.Min(intB, intC));
        }
    }
}