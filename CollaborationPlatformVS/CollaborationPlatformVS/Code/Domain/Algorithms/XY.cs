﻿using System.Collections.Generic;

namespace CollaborationPlatformVS.Code.Domain.Algorithms
{
    public class XY
    {
        public List<Point> XyList;
        public int Size { get; private set; }

        public XY(int defSize = 9)
        {
            Size = 0;
            XyList = new List<Point>(defSize);
        }

        public void Clear() => Size = 0;

        public Point Get(int index) => XyList[index];

        public void Add(int x, int y)
        {
            if (Size == XyList.Count) XyList.Add((new Point(0, 0)));
            XyList[Size].X = x;
            XyList[Size].Y = y;
            Size++;
        }
    }

    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}