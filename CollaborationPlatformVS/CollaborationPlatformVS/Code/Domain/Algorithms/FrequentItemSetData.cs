﻿using System.Collections.Generic;

namespace CollaborationPlatformVS.Code.Domain.Algorithms
{
    public class FrequentItemSetData<TAttribute>
    {
        public List<HashSet<TAttribute>> FrequentItemsetList { get; }
        public Dictionary<HashSet<TAttribute>, int> SupportCountMap { get; }
        public double MinimumSupport { get; }
        public int NumberOfTransactions { get; }

        public FrequentItemSetData(List<HashSet<TAttribute>> frequentItemsetList, Dictionary<HashSet<TAttribute>, int> supportCountMap,
            double minimumSupport, int numberOfTransactions)
        {
            FrequentItemsetList = frequentItemsetList;
            SupportCountMap = supportCountMap;
            MinimumSupport = minimumSupport;
            NumberOfTransactions = numberOfTransactions;
        }

        public double GetSupport(HashSet<TAttribute> itemset)
        {
            if (SupportCountMap.ContainsKey(itemset)) return 1.0 * SupportCountMap[itemset] / NumberOfTransactions;
            return 0;
        }
    }
}