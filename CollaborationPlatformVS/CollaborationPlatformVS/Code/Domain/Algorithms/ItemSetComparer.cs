﻿using System.Collections.Generic;
using System.Linq;
using CollaborationPlatformVS.Code.Domain.ClientServerDataObjects;

namespace CollaborationPlatformVS.Code.Domain
{
    public class ItemSetComparer : IEqualityComparer<HashSet<Attribute>>
    {
        public bool Equals(HashSet<Attribute> x, HashSet<Attribute> y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (ReferenceEquals(null, x)) return false;
            if (ReferenceEquals(null, y)) return false;
            return x.SetEquals(y);
        }

        public int GetHashCode(HashSet<Attribute> obj)
        {
            return obj.Aggregate(0, (current, myClass) => (current * 397) ^ myClass?.GetHashCode() ?? 0);
        }
    }
}