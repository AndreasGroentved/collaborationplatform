﻿using System;
using System.Collections.Generic;
using System.Linq;
using Attribute = CollaborationPlatformVS.Code.Domain.ClientServerDataObjects.Attribute;

namespace CollaborationPlatformVS.Code.Domain.Algorithms
{
    public class Apriori
    {
        private Dictionary<HashSet<Attribute>, HashSet<Attribute>> _rules;

        public FrequentItemSetData<Attribute> Generate(List<HashSet<Attribute>> transactionList, double minimumSupport)
        {
            if (transactionList == null || transactionList.Count == 0) return null;
            CheckSupport(minimumSupport);

            Dictionary<HashSet<Attribute>, int> supportCountMap = new Dictionary<HashSet<Attribute>, int>(new ItemSetComparer());

            // Get the list of 1-itemsets that are frequent
            List<HashSet<Attribute>> frequentItemList = FindFrequentItems(transactionList, supportCountMap, minimumSupport);

            // Maps each 'k' to the list of frequent k-itemsets
            Dictionary<int, List<HashSet<Attribute>>> map = new Dictionary<int, List<HashSet<Attribute>>>
            {
                {1, frequentItemList} // map.Add(1, frequentItemList);
            };

            // 'k' denotes the cardinality of itemsets processed at each iteration
            // of the following loop
            int k = 1;
            do
            {
                ++k;

                // First generate the candidates
                List<HashSet<Attribute>> candidateList = GenerateCandidates(map[k - 1]);

                foreach (HashSet<Attribute> transaction in transactionList)
                {
                    List<HashSet<Attribute>> candidateList2 = Subset(candidateList, transaction);

                    foreach (HashSet<Attribute> itemset in candidateList2)
                    {
                        if (supportCountMap.ContainsKey(itemset)) supportCountMap[itemset] = supportCountMap[itemset] + 1;
                        else supportCountMap.Add(itemset, 1);
                    }
                }
                map.Add(k, GetNextItemsets(candidateList, supportCountMap, minimumSupport, transactionList.Count));
            } while (map[k].Count != 0);

            return new FrequentItemSetData<Attribute>(ExtractFrequentItemsets(map), supportCountMap, minimumSupport, transactionList.Count);
        }

        public Dictionary<HashSet<Attribute>, HashSet<Attribute>> GenerateRules(FrequentItemSetData<Attribute> frequentItemsets, double confidence)
        {
            ItemSetComparer comparer = new ItemSetComparer();
            // Rules will be returned in a map with the attribute name as the key and a set of attributes as the value
            _rules = new Dictionary<HashSet<Attribute>, HashSet<Attribute>>(new ItemSetComparer());

            // Generate 1-1 association rules
            foreach (HashSet<Attribute> itemset in frequentItemsets.FrequentItemsetList)
            {
                foreach (Attribute a in itemset)
                {
                    foreach (Attribute b in itemset)
                    {
                        if (a.Equals(b)) continue;
                        // If a and b are not the same, add the a->b assocation to the rules dictionary
                        HashSet<Attribute> lhs = new HashSet<Attribute> {a}; // Left Hand Side
                        HashSet<Attribute> rhs = new HashSet<Attribute> {b}; // Right Hand Side
                        HashSet<Attribute> combined = new HashSet<Attribute> {a, b}; // Combined - used for confidence checking

                        // Confidence checking
                        if (!frequentItemsets.SupportCountMap.ContainsKey(combined) || !frequentItemsets.SupportCountMap.ContainsKey(lhs)) continue;
                        double tempConfidence = 1.0 * frequentItemsets.SupportCountMap[combined] / frequentItemsets.SupportCountMap[lhs];
                        if (!(tempConfidence >= confidence)) continue;
                        // Rule seems ok, add it
                        if (_rules.ContainsKey(lhs)) _rules[lhs].UnionWith(rhs); // Union lhs' value (rhs) with the rhs set
                        else _rules.Add(lhs, rhs);
                    }
                }
            }
            return _rules;
        }

        private void GenerateRulesSub(HashSet<Attribute> itemset)
        {
            List<Attribute> itemsetList = new List<Attribute>(itemset);
            int size = itemset.Count;

            // No rule needed for itemsets of count 1 or less
            if (size < 3) return;

            for (int i = 0; i < size; i++)
            {
                // Compute the left hand side
                HashSet<Attribute> leftHandSide = new HashSet<Attribute>();
                for (int j = 0; j <= i; j++) leftHandSide.Add(itemsetList[j]);

                // If all items are in the LHS, skip this run
                if (leftHandSide.Count == size) continue;

                // Compute the right hand side
                HashSet<Attribute> rightHandSide = new HashSet<Attribute>();
                for (int j = leftHandSide.Count; j < size; j++) rightHandSide.Add(itemsetList[j]);

                _rules.Add(leftHandSide, rightHandSide);
            }
        }

        private List<HashSet<Attribute>> ExtractFrequentItemsets(Dictionary<int, List<HashSet<Attribute>>> map)
        {
            List<HashSet<Attribute>> ret = new List<HashSet<Attribute>>();
            foreach (List<HashSet<Attribute>> itemsetList in map.Values) ret.AddRange(itemsetList);
            return ret;
        }

        // This method gathers all the frequent candidate itemsets into a single list
        private List<HashSet<Attribute>> GetNextItemsets(List<HashSet<Attribute>> candidateList,
            Dictionary<HashSet<Attribute>, int> supportCountMap, double minimumSupport, int transactions)
        {
            List<HashSet<Attribute>> ret = new List<HashSet<Attribute>>(candidateList.Count);

            foreach (HashSet<Attribute> itemset in candidateList)
            {
                if (supportCountMap.ContainsKey(itemset))
                {
                    int supportCount = supportCountMap[itemset];
                    double support = 1.0 * supportCount / transactions;
                    if (support >= minimumSupport) ret.Add(itemset);
                }
            }

            return ret;
        }

        private List<HashSet<Attribute>> Subset(List<HashSet<Attribute>> candidateList, HashSet<Attribute> transaction)
        {
            List<HashSet<Attribute>> ret = new List<HashSet<Attribute>>(candidateList.Count);
            ret.AddRange(candidateList.Where(candidate => candidate.All(a => transaction.Contains(a))));
            return ret;
        }

        // Generates the next candidates. This is the so called F_(k - 1) x F_(k - 1) method
        private List<HashSet<Attribute>> GenerateCandidates(List<HashSet<Attribute>> itemsetList)
        {
            List<List<Attribute>> list = new List<List<Attribute>>(itemsetList.Count);
            foreach (HashSet<Attribute> itemset in itemsetList)
            {
                List<Attribute> l = new List<Attribute>(itemset); // Convert the itemset to a list
                l.Sort();
                list.Add(l);
            }

            int listSize = list.Count;

            List<HashSet<Attribute>> ret = new List<HashSet<Attribute>>(listSize);

            for (int i = 0; i < listSize; i++)
            {
                for (int j = i + 1; j < listSize; j++)
                {
                    HashSet<Attribute> candidate = TryMergeItemsets(list[i], list[j]);
                    if (candidate != null) ret.Add(candidate);
                }
            }
            return ret;
        }

        // Attempts the actual construction of the next itemset candidate.
        private HashSet<Attribute> TryMergeItemsets(List<Attribute> itemset1, List<Attribute> itemset2)
        {
            int length = itemset1.Count;
            for (int i = 0; i < length - 1; i++)
            {
                if (!itemset1[i].Equals(itemset2[i])) return null;
            }

            if (itemset1[length - 1].Equals(itemset2[length - 1])) return null;

            HashSet<Attribute> ret = new HashSet<Attribute>();
            for (int i = 0; i < length - 1; i++) ret.Add(itemset1[i]);

            ret.Add(itemset1[length - 1]);
            ret.Add(itemset2[length - 1]);
            return ret;
        }

        private List<HashSet<Attribute>> FindFrequentItems(List<HashSet<Attribute>> itemsetList,
            Dictionary<HashSet<Attribute>, int> supportCountMap,
            double minimumSupport)
        {
            Dictionary<Attribute, int> map = new Dictionary<Attribute, int>();

            // Count the support counts of each item
            foreach (HashSet<Attribute> itemset in itemsetList)
            {
                foreach (Attribute att in itemset)
                {
                    HashSet<Attribute> tmp = new HashSet<Attribute> {att};

                    if (supportCountMap.ContainsKey(tmp)) supportCountMap[tmp] = supportCountMap[tmp] + 1;
                    else supportCountMap.Add(tmp, 1);
                    if (map.ContainsKey(att)) map[att] = map[att] + 1;
                    else map.Add(att, 1);
                }
            }

            List<HashSet<Attribute>> frequentItemsetList = new List<HashSet<Attribute>>();

            foreach (KeyValuePair<Attribute, int> kvp in map)
            {
                if (!(1.0 * kvp.Value / map.Count >= minimumSupport)) continue;
                HashSet<Attribute> itemset = new HashSet<Attribute> {kvp.Key};
                frequentItemsetList.Add(itemset);
            }

            return frequentItemsetList;
        }

        private void CheckSupport(double support)
        {
            if (Double.IsNaN(support)) throw new ArgumentException("The input support is NaN.");
            if (support > 1.0) throw new ArgumentException("The input support is too large: " + support + ", should be at most 1.0");
            if (support < 0.0) throw new ArgumentException("The input support is too small: " + support + ", should be at least 0.0");
        }
    }
}