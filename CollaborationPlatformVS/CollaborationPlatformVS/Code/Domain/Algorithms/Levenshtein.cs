﻿using System;

namespace CollaborationPlatformVS.Code.Domain.Algorithms
{
    public static class Levenshtein
    {
        //TODO stjålet uoptimeret algoritme...... https://fuzzystring.codeplex.com/SourceControl/latest#FuzzyString/FuzzyString/HammingDistance.cs

        public static int LevenshteinDistance(this string source, string target) => LevenshteinDistanceC(source.ToLower(), target.ToLower());
        
        public static int LevenshteinDistanceC(string source, string target)
        {
            if (source.Length == 0) return target.Length;
            if (target.Length == 0) return source.Length;

            int distance = source[source.Length - 1] == target[target.Length - 1] ? 0 : 1;

            return Math.Min(Math.Min(LevenshteinDistanceC(source.Substring(0, source.Length - 1), target) + 1,
                                     LevenshteinDistanceC(source, target.Substring(0, target.Length - 1))) + 1,
                                     LevenshteinDistanceC(source.Substring(0, source.Length - 1), target.Substring(0, target.Length - 1)) + distance);
        }       
    }
}