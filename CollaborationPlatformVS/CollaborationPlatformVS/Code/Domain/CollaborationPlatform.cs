﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using CollaborationPlatformVS.Code.Connection;
using CollaborationPlatformVS.Code.Domain.ClientServerDataObjects;
using CollaborationPlatformVS.Code.Domain.Managers;
using CollaborationPlatformVS.Code.Security;
using Newtonsoft.Json;
using Attribute = CollaborationPlatformVS.Code.Domain.ClientServerDataObjects.Attribute;

namespace CollaborationPlatformVS.Code.Domain
{
    public class CollaborationPlatform : ICollaborate
    {
        private readonly IUserManager _userManager;
        private readonly Dictionary<string, MethodDelegate> _methods;
        private readonly WebSocketConnectionManager _wscm;
        private IDisposable _surveySubject;
        private readonly IEncrypt _iEncrypt;
        private readonly IUserSurveyHandler _userSurveyHandler;
        private readonly IAttributeHandler _attributeHandler;

        private delegate Task<InstructionClient> MethodDelegate(string json, string login);

        public CollaborationPlatform(WebSocketConnectionManager web, IUserManager userManager,
            IEncrypt iEncrypt, IUserSurveyHandler userSurveyHandler, IAttributeHandler attributeHandler)
        {
            _wscm = web;
            MessageSubject = new Subject<SocketMessage>();
            _userManager = userManager;
            _iEncrypt = iEncrypt;
            _userSurveyHandler = userSurveyHandler;
            _attributeHandler = attributeHandler;
            _surveySubject = _userSurveyHandler.GetSurveySubject().Subscribe(UpdateSurveysForConnectedUsers);
            _methods = new Dictionary<string, MethodDelegate> //Alle metoder accepterer JSON og returnerer JSON
            {
                {"Login", Login},
                {"CreateUser", CreateUser},
                {"UpdateUser", UpdateUser},
                {"CreateUserSurvey", CreateUserSurvey},
                {"UpdateUserSurvey", UpdateUserSurvey},
                {"GetUserSurveys", GetUserSurveys},
                {"AttributeSearch", AttributesSearch},
                {"SearchRelated", SearchRelated},
                {"GetSearchAttributeCount", GetSearchAttributeCount}
            };
        }

        //Input fra klienten er enten ren jsontekst, krypteret med aes eller krypteret med rsa, rsa bruges til kryptering af 
        //password ved oprettelse af brugere. Email er krypteret med RSA, i teorien kan forespørgsler overvåges og gentages,
        //dette er accepteret her, et alternativ kunne være at få et id fra serveren og kræve at serveren holder styr på,
        //hvem der er logget ind, brugeren ville så få socketId, som for hver request ville skulle matches med det socketId,
        //der er tildelt forbindelsen
        public async Task<string> Receive(string input, string socketId)
        {
            try
            {
                InstructionServer jsonObject = JsonConvert.DeserializeObject<InstructionServer>(input);
                Message message;
                string email = jsonObject.Email;
                string password = "";

                if (jsonObject.Email.Length != 0 && !jsonObject.Email.Equals("RSA")) //Krypteret aes
                {
                    email = _iEncrypt.RsaDecrypt(jsonObject.Email);
                    var jsonMessage = AesEncrypted(ref email, jsonObject, ref password);
                    if (jsonMessage.Equals("keyError")) return "decryption error";
                    message = JsonConvert.DeserializeObject<Message>(jsonMessage);
                }
                else message = JsonConvert.DeserializeObject<Message>(jsonObject.Message.ToString());
                if (jsonObject.Email.Equals("RSA")) //Krypteret RSA
                {
                    RsaEncrypted(message, ref email, ref password);
                }

                //UpdateSocketId(socketId, email);
                InstructionClient instruction = await _methods[message.MethodName](message.Argument.ToString(), email);
                instruction.CallId = jsonObject.CallId;

                if (email.Length > 0) //Hvis email kendes, skal returobjektet krypteres
                {
                    instruction.Argument = _iEncrypt.Encrypt(JsonConvert.SerializeObject(instruction.Argument), password);
                }
                return JsonConvert.SerializeObject(instruction);
            }

            catch (Exception e)
            {
                Debug.WriteLine("Exception: " + e.Message);
                return "Exception: " + e.Message;
            }
        }

        private string AesEncrypted(ref string email, InstructionServer jsonObject, ref string password) //Ref for at spare oprettelse af nyt objekt
        {
            password = _userManager.GetPasswordForEncryption(email);
            return _iEncrypt.Decrypt(jsonObject.Message.ToString(), password);
        }

        private void RsaEncrypted(Message message, ref string email, ref string password)
        {
            Registration reg = JsonConvert.DeserializeObject<Registration>(JsonConvert.SerializeObject(message.Argument));
            reg.Password = _iEncrypt.RsaDecrypt(reg.Password);
            User user = JsonConvert.DeserializeObject<User>(_iEncrypt.Decrypt(reg.User.ToString(), reg.Password));
            email = user.Email;
            reg.User = user;
            password = reg.Password;
            message.Argument = JsonConvert.SerializeObject(reg);
        }

        public Subject<SocketMessage> MessageSubject { get; }

        private void UpdateSurveysForConnectedUsers(SurveyUser surveyUser) //TODO ikke en prioritet
        {
            if (!_wscm.UserConnected(surveyUser.User)) return; //TODO encrypt
            SurveyData surveyData = surveyUser.SurveyData; //TODO kode klon
            InstructionClient instructionClient = new InstructionClient("", new Message("GetUserSurvey", surveyData));
            string returnString = JsonConvert.SerializeObject(instructionClient);
            returnString = _iEncrypt.Encrypt(returnString, _userManager.GetPasswordForEncryption(surveyUser.User));
            MessageSubject.OnNext(new SocketMessage(surveyUser.User, returnString));
        }

        private void UpdateSocketId(string socketId, string email)
        {
            if (socketId != email) _wscm.UpdateSocketIdToUserID(socketId, email);
        }

        public async Task<InstructionClient> CreateUserSurvey(string json, string email)
        {
            SurveySearch surveySearch = JsonConvert.DeserializeObject<SurveySearch>(json);
            string id = await _userSurveyHandler.CreateSurvey(surveySearch);
            return new InstructionClient("", new Message("CreateUserSurvey", id));
        }

        public async Task<InstructionClient> GetSearchAttributeCount(string json, string email)
        {
            SearchAttribute[] searchAttributes = JsonConvert.DeserializeObject<SearchAttribute[]>(json);
            Dictionary<string, int> searchAttributeCounts = await _userSurveyHandler.GetSearchAttributeCount(searchAttributes);
            return new InstructionClient("", new Message("GetSearchAttributeCount", searchAttributeCounts));
        }

        public async Task<InstructionClient> SearchRelated(string json, string email)
        {
            List<Attribute> attrList = JsonConvert.DeserializeObject<List<Attribute>>(json);
            return new InstructionClient("", new Message("SearchRelated", await _attributeHandler.GetFrequentList(attrList)));
        }

        public async Task<InstructionClient> AttributesSearch(string json, string email)
        {
            AttributeSearch attributeSearch = await _attributeHandler.GetAutoCompleteAndCorrectedAttributeList(json);
            return new InstructionClient("", new Message("AttributeSearch", attributeSearch));
        }


        public async Task<InstructionClient> GetUserSurveys(string json, string email)
        {
            List<SurveySearch> surveys = await _userSurveyHandler.GetSurveys(email);
            return new InstructionClient("", new Message("GetUserSurveys", surveys));
        }

        public async Task<InstructionClient> UpdateUserSurvey(string json, string email)
        {
            SurveySearch surveyData = JsonConvert.DeserializeObject<SurveySearch>(json);
            bool updated = await _userSurveyHandler.UpdateSurvey(email, surveyData);
            return new InstructionClient("", new Message("UpdateUserSurvey", updated));
        }

        public async Task<InstructionClient> CreateUser(string json, string email)
        {
            Registration registration = JsonConvert.DeserializeObject<Registration>(json);
            User user = JsonConvert.DeserializeObject<User>(registration.User.ToString());
            bool created = await _userManager.CreateUser(user, registration.Password);
            return new InstructionClient("", new Message("CreateUser", created));
        }

        public async Task<InstructionClient> UpdateUser(string json, string email)
        {
            User user = JsonConvert.DeserializeObject<User>(json);
            bool updated = await _userManager.UpdateUser(user, email);
            return new InstructionClient("", new Message("UpdateUser", updated));
        }

        public async Task<InstructionClient> Login(string json, string email)
        {
            Login loginObject = JsonConvert.DeserializeObject<Login>(json);
            User user = await _userManager.IsLoginValid(loginObject);
            return new InstructionClient("", new Message("Login", user));
        }
    }
}