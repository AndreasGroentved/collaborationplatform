﻿using System.Reactive.Subjects;
using System.Threading.Tasks;
using CollaborationPlatformVS.Code.Util;

namespace CollaborationPlatformVS.Code.Domain
{
    public interface ICollaborate
    {
        Task<string> Receive(string input, string socketId);
        Subject<SocketMessage> MessageSubject { get; }
    }
}