﻿using System.Collections.Generic;
using CollaborationPlatformVS.Code.Domain.ClientServerDataObjects;

namespace CollaborationPlatformVS.Code.Domain.DataObjects
{
    public class AttributePeople
    {
        public List<User> Users; //TODO optimering -ikke prioritet
        public SearchAttribute Attribute;

        public AttributePeople(SearchAttribute a)
        {
            Users = new List<User>();
            Attribute = a;
        }

        public AttributePeople(User u, SearchAttribute a)
        {
            Users = new List<User> {u};
            Attribute = a;
        }
    }
}