﻿using System.Collections.Generic;
using System.Linq;
using CollaborationPlatformVS.Code.Data;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace CollaborationPlatformVS.Code.Domain.ClientServerDataObjects
{
    public class SurveySearchResult
    {
        [BsonId]
        [JsonIgnore]
        public ObjectId Id { get; set; }

        [BsonElement("peopleWithAttributes")]
        public Dictionary<string, int> PeopleWithAttributes { get; set; }

        [BsonElement("surveySearch")]
        public SurveySearch SurveySearch { get; set; }

        [BsonElement("users")]
        public List<ParticipateDTO> Users { get; set; }

        public SurveySearchResult(List<UserDTO> u, SurveySearch s, Dictionary<string, int> d)
        {
            Users = u.Select(user => new ParticipateDTO {Email = user.Email, HasAnswered = false, Participates = false}).ToList();
            SurveySearch = s;
            PeopleWithAttributes = d;
        }

        public SurveySearchResult(List<ScorePeople> score, SurveySearch s, Dictionary<string, int> d)
        {
            
            Users = score.Select(user => new ParticipateDTO {Email = user.User.Email, HasAnswered = false, Participates = false}).ToList();
            SurveySearch = s;
            PeopleWithAttributes = d;
        }

        public SurveySearchResult()
        {
        }

        public override string ToString()
        {
            return $"{nameof(Users)}: {Users}, {nameof(SurveySearch)}: {SurveySearch}";
        }
    }
}