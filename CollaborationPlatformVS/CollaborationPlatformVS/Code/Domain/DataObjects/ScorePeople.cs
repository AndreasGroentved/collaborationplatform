﻿using CollaborationPlatformVS.Code.Domain.ClientServerDataObjects;

namespace CollaborationPlatformVS.Code.Data
{
    public class ScorePeople
    {
        public User User;
        public int Score;

        public ScorePeople(User u, int s)
        {
            User = u;
            Score = s;
        }
    }
}
