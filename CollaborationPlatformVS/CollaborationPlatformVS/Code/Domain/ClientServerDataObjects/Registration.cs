﻿using System;

namespace CollaborationPlatformVS.Code.Domain.ClientServerDataObjects
{
    public class Registration
    {
        public Object User { get; set; } //TODO hmm
        public string Password { get; set; }

        public Registration() { }

        public Registration(string user, string password )
        {
            User = user;
            Password = password;
        }
    }
}
