﻿using System.Collections.Generic;

namespace CollaborationPlatformVS.Code.Domain.ClientServerDataObjects
{
    public class User
    {
        //TODO alder skal nok være en del af user og ikke en attribut
        public List<Attribute> Attributes { get; set; }

        public string Email { get; set; }
        public string Gender { get; set; } /* { can only be "Male" or "Female" } */
        public string Name { get; set; }


        public User()
        {
            
        }

        public User(List<Attribute> attributes, string email, string name, string gender = "intet")
        {
            Attributes = attributes;
            Email = email;
            Name = name;
            Gender = gender;
        }

        private static User _nullUserObject;

        public static User NullUser() => _nullUserObject ?? (_nullUserObject = new User(new List<Attribute>(), "", ""));

        public bool EditInformation(List<Attribute> updatedAttributes = null, string userName = "")
        {
            return UpdateName(userName) || UpdateAttributes(updatedAttributes);
        }

        private bool UpdateName(string userName)
        {
            if (userName.Length <= 0 || userName.Equals(Name)) return false;
            Name = userName;
            return true;
        }

        private bool UpdateAttributes(List<Attribute> attributes)
        {
            if (attributes == null || Attributes.Equals(attributes)) return false;

            Attributes = attributes;
            return true;
        }

        public override string ToString()
        {
            return $"{nameof(Attributes)}: {Attributes}, {nameof(Email)}: {Email}";
        }

        public override bool Equals(object obj)
        {
            User o = obj as User;
            if (o?.Attributes.Count != Attributes.Count) return false;

            for (int i = 0; i < Attributes.Count; i++)
            {
                if (Attributes[i] != o.Attributes[i]) return false; // Return false if any of the attributes do not match
            }
            return Email.Equals(o.Email);
        }

        public override int GetHashCode()
        {
            var hashCode = Email?.GetHashCode() ?? 0;
            return hashCode;
        }
    }
}