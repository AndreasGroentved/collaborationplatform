﻿namespace CollaborationPlatformVS.Code.Domain.ClientServerDataObjects
{
    public class InstructionClient
    {

        public string CallId { get; set; }
        
        public object Argument { get; set; }

        public InstructionClient(string callId, object argument)
        {
            CallId = callId;
            Argument = argument;
        }
    }
}
