﻿namespace CollaborationPlatformVS.Code.Domain.ClientServerDataObjects
{
    public enum Type
    {
        INTEGER,
        FLOAT,
        BOOLEAN,
        STRING
    }

}
