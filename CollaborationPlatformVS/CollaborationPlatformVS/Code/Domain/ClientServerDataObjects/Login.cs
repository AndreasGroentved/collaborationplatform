﻿namespace CollaborationPlatformVS.Code.Domain.ClientServerDataObjects
{
    public class Login
    {
        public string Password { get; set; }

        public string Email { get; set; }

        public Login(string email, string password)
        {
            Password = password;
            Email = email;
        }

        protected bool Equals(Login other) => string.Equals(Password, other.Password) && string.Equals(Email, other.Email);

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Login) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Password?.GetHashCode() ?? 0) * 397) ^ (Email?.GetHashCode() ?? 0);
            }
        }

        public static bool operator ==(Login left, Login right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Login left, Login right)
        {
            return !Equals(left, right);
        }
    }
}