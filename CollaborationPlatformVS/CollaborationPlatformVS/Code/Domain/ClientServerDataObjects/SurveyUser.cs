﻿namespace CollaborationPlatformVS.Code.Domain.ClientServerDataObjects
{
    public class SurveyUser
    {
        public SurveyData SurveyData { get; set; }
        public string User { get; set; }
        
        public SurveyUser(SurveyData sData, string user)
        {
            SurveyData = sData;
            User = user;
        }
    }
}
