﻿namespace CollaborationPlatformVS.Code.Domain.ClientServerDataObjects
{
    public class Message
    {
        public string MethodName { get; set; }
        public object Argument { get; set; }


        public Message(string methodName, object argument)
        {
            MethodName = methodName;
            Argument = argument;
        }
    }
}
