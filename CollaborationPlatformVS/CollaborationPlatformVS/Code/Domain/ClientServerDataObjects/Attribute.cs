﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace CollaborationPlatformVS.Code.Domain.ClientServerDataObjects
{
    public class Attribute : IComparable<Attribute>
//TODO sørge for at attributer i undersøgelser er de samme, som dem, der bliver gemt for sig
    {
        [BsonId]
        [JsonIgnore]
        public ObjectId Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("value")]
        public string Value { get; set; }

        [BsonElement("type")]
        public Type Type { get; set; }

        [BsonElement("count")]
        public int Count { get; set; }


        public Attribute(ObjectId id, string name, string value, Type type, int count)
        {
            Id = id;
            Value = value ?? throw new ArgumentNullException(nameof(value));
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Type = type;
            Count = count;
        }

        public Attribute()
        {
        }

        private static Attribute NullAttributeObject { get; set; }

        public static Attribute NullAttribute()
        {
            return NullAttributeObject ?? (NullAttributeObject = new Attribute {Count = -1, Id = ObjectId.Empty, Name = "", Value = ""});
        }

        protected bool Equals(Attribute other) => Name.Equals(other.Name, StringComparison.CurrentCultureIgnoreCase);

        public IEnumerator<Attribute> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Attribute) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Name?.GetHashCode() ?? 0;
                return hashCode;
            }
        }


        public int CompareTo(Attribute other)
        {
            return String.Compare(Name, other.Name, StringComparison.OrdinalIgnoreCase);
        }


        public static bool operator ==(Attribute left, Attribute right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Attribute left, Attribute right)
        {
            return !Equals(left, right);
        }
    }

    class AttributeComparer : IEqualityComparer<Attribute>
    {
        // Products are equal if their names and product numbers are equal.
        public bool Equals(Attribute x, Attribute y)
        {
            //Check whether the compared objects reference the same data.
            if (ReferenceEquals(x, y)) return true;

            //Check whether any of the compared objects is null.
            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
                return false;

            //Check whether the products' properties are equal.
            return x.Name.Equals(y.Name);
        }

        // If Equals() returns true for a pair of objects 
        // then GetHashCode() must return the same value for these objects.

        public int GetHashCode(Attribute attribute)
        {
            //Check whether the object is null
            if (ReferenceEquals(attribute, null)) return 0;

            //Calculate the hash code for the product.
            return attribute.GetHashCode();
        }
    }
}