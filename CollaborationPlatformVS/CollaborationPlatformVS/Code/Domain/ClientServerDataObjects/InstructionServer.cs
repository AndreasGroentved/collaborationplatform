﻿namespace CollaborationPlatformVS.Code.Domain.ClientServerDataObjects
{
    public class InstructionServer
    {
        public string Email { get; set; }
        public object Message { get; set; }
        public string CallId { get; set; }

        public InstructionServer(string email, object message, string callId)
        {
            Email = email;
            Message = message;
            CallId = callId;
        }

        public InstructionServer()
        {
        }
    }
}