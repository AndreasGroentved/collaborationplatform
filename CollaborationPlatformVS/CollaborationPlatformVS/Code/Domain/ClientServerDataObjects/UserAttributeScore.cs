﻿using System.Collections.Generic;
using CollaborationPlatformVS.Code.Data;

namespace CollaborationPlatformVS.Code.Domain.ClientServerDataObjects
{
    public class UserAttributeScore
    {
        public List<ScorePeople> ScorePeoples { get; set; }
        public Dictionary<Attribute, int> AttributeCount { get; set; }

        public UserAttributeScore(List<ScorePeople> scorePeoples, Dictionary<Attribute, int> attributeCount)
        {
            ScorePeoples = scorePeoples;
            AttributeCount = attributeCount;
        }
    }
}
