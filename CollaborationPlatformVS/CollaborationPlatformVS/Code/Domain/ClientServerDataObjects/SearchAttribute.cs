﻿using MongoDB.Bson.Serialization.Attributes;

namespace CollaborationPlatformVS.Code.Domain.ClientServerDataObjects
{
    public class SearchAttribute
    {
        [BsonElement("minVal")]
        public int MinVal { get; set; }

        [BsonElement("maxVal")]
        public int MaxVal { get; set; }

        [BsonElement("attribute")]
        public Attribute Attribute { get; set; } //= Attribute.NullAttribute();

        [BsonElement("must")]
        public bool Must { get; set; }

        [BsonElement("freq")]
        public int Frequency { get; set; }

        [BsonElement("not")]
        public bool Not { get; set; }

    }
}
