﻿using System;

namespace CollaborationPlatformVS.Code.Domain.ClientServerDataObjects
{

    public class SurveySearch
    {
        public string Id { get; set; }
        public SearchAttribute[] Attributes { get; set; }
        public string User { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int NumberOfUsers { get; set; }
        public int NumberConfirmed { get; set; }
        public int NumberDenied { get; set; }
        public bool ParticipationFlag { get; set; } //Færre objekter og forespørgsler...
        public string Description { get; set; }

        public SurveySearch()
        {
            Id = "-1";
            Attributes = new SearchAttribute[0];
            User = "-1";
            FromDate = new DateTime();
            ToDate = new DateTime();
            NumberOfUsers = 0;
            NumberDenied = 0;
            NumberConfirmed = 0;
            ParticipationFlag = false; 
            Description = "";
        }

        public override string ToString()
        {
            return $"{nameof(Id)}: {Id}, {nameof(Attributes)}: {Attributes}, {nameof(User)}: {User}, {nameof(FromDate)}: {FromDate}, {nameof(ToDate)}: {ToDate}, {nameof(NumberOfUsers)}: {NumberOfUsers}, {nameof(NumberConfirmed)}: {NumberConfirmed}, {nameof(NumberDenied)}: {NumberDenied}, {nameof(ParticipationFlag)}: {ParticipationFlag}, {nameof(Description)}: {Description}";
        }
    }
}