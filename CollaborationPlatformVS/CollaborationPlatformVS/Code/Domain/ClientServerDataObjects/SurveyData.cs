﻿using System;

namespace CollaborationPlatformVS.Code.Domain.ClientServerDataObjects
{
    public class SurveyData
    {
        public string Id;
        public Attribute[] Attributes;
        public string User;
        public DateTime FromDate;
        public DateTime ToDate;
        public int NumberOfUsers;
        public int NumberConfirmed;
        public int NumberDenied;
        public bool ParticipationFlag;
        public string Description;


        public SurveyData(string id, Attribute[] attributes, string user, DateTime fromDate, DateTime toDate, int numberOfUsers, int numberConfirmed, int numberDenied, bool participationFlag, string description)
        {
            Id = id;
            Attributes = attributes;
            User = user;
            FromDate = fromDate;
            ToDate = toDate;
            NumberOfUsers = numberOfUsers;
            NumberConfirmed = numberConfirmed;
            NumberDenied = numberDenied;
            ParticipationFlag = participationFlag;
            Description = description;
        }

        public SurveyData()
        {
            Id = "-1";
            Attributes = new Attribute[0];
            User = "-1";
            FromDate = new DateTime();
            ToDate = new DateTime();
            NumberOfUsers = 0;
            NumberDenied = 0;
            NumberConfirmed = 0;
            ParticipationFlag = false;
            Description = "";
        }
    }
}