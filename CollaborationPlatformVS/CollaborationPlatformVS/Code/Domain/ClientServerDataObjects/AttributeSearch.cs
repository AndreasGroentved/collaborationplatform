﻿using System.Collections.Generic;

namespace CollaborationPlatformVS.Code.Domain.ClientServerDataObjects
{
    public class AttributeSearch
    {
        public List<string> AutoCompleteList { get; set; }
        public List<string> CorrectionList { get; set; }

        public AttributeSearch()
        {
            AutoCompleteList = new List<string>();
            CorrectionList = new List<string>();//TODO lav til en liste
        }

        public AttributeSearch(List<string> autoCompleteList, List<string> correctionList)
        {
            AutoCompleteList = autoCompleteList;
            CorrectionList = correctionList;
        }
    }
}
