﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CollaborationPlatformVS.Code.Data;
using CollaborationPlatformVS.Code.Domain.Algorithms;
using CollaborationPlatformVS.Code.Domain.ClientServerDataObjects;
using Attribute = CollaborationPlatformVS.Code.Domain.ClientServerDataObjects.Attribute;

namespace CollaborationPlatformVS.Code.Domain.Managers
{
    public class AttributeHandler : IAttributeHandler
    {
        private readonly IDataManager _dataManager;
        private readonly ConcurrentDictionary<int, HashSet<Attribute>> _ruleLengthDictionary;
        private readonly ConcurrentDictionary<int, ConcurrentBag<HashSet<Attribute>>> _ruleDictionary;
        private const long UPDATE_INTERVAL = 3600000; //Opdater hver time
        private readonly Stopwatch _stop;
        private static volatile bool _isUpdating = false;

        public AttributeHandler(IDataManager dataManager)
        {
            _dataManager = dataManager;
            _ruleLengthDictionary = new ConcurrentDictionary<int, HashSet<Attribute>>();
            _ruleDictionary = new ConcurrentDictionary<int, ConcurrentBag<HashSet<Attribute>>>();
            _stop = new Stopwatch();
            _stop.Start();
            bool a = BuildRelatedDictionaries().Result;
        }

        private async Task<bool> BuildRelatedDictionaries()
        {
            lock (this)
            {
                if (_isUpdating) return false;
                _isUpdating = true;
                _stop.Restart();
            }

            Apriori a = new Apriori();
            List<HashSet<Attribute>> attributeSets = await _dataManager.GetAttributeSets();

            FrequentItemSetData<Attribute> data = a.Generate(attributeSets, 0.01); //TODO caching + invalidering
            if (data != null)
            {
                foreach (HashSet<Attribute> set in data.FrequentItemsetList)
                {
                    if (!_ruleLengthDictionary.ContainsKey(set.Count))
                        _ruleLengthDictionary[set.Count] = new HashSet<Attribute>();
                    _ruleLengthDictionary[set.Count].UnionWith(set);
                    if (!_ruleDictionary.ContainsKey(set.Count))
                        _ruleDictionary[set.Count] = new ConcurrentBag<HashSet<Attribute>>();
                    if (_ruleDictionary[set.Count].Contains(set)) continue;
                    _ruleDictionary[set.Count].Add(set);
                }
            }

            _isUpdating = false;
            return true;
        }

        public async Task<List<Attribute>> GetFrequentList(List<Attribute> attributes)
        {
            if (_stop.ElapsedMilliseconds > UPDATE_INTERVAL) //TODO der er selvfølgeligt nok muligvis en bedre måde at gøre det på
            {
                BuildRelatedDictionaries();
            }

            if (!attributes.Any()) return new List<Attribute>();
            HashSet<Attribute> userAttr = new HashSet<Attribute>(attributes);
            return GetRelatedAttributes(userAttr).ToList();
        }

        private HashSet<Attribute> GetRelatedAttributes(HashSet<Attribute> userAttr)
        {
            HashSet<Attribute> returnAttributes = new HashSet<Attribute>();

            for (int i = 2; i <= userAttr.Count; i++)
            {
                if (!_ruleLengthDictionary.ContainsKey(i) || _ruleLengthDictionary.Count == 0) continue; //ingen regler for længde
                if (userAttr.Intersect(_ruleLengthDictionary[i]).Count() < i - 1) continue; //mindre antal attributter end regler, der matches med

                foreach (HashSet<Attribute> set in _ruleDictionary[i])
                {
                    IEnumerable<Attribute> intersection = userAttr.Intersect(set);
                    if (intersection.Count() == i - 1) //Mangler et i frekvent itemset - returner manglende værdi
                    {
                        returnAttributes.Add(set.Except(intersection).First()); //Dette if statement rammes kun,hvis dette er et
                    }
                }
            }
            return returnAttributes;
        }


        public async Task<AttributeSearch> GetAutoCompleteAndCorrectedAttributeList(string searchValue)
        {
            List<string> autoCompleteList = new List<string>();
            List<String> aprioriList = new List<string>();
            List<Attribute> allAttributes = _dataManager.GetAllAttributes();
            allAttributes.ForEach(a =>
            {
                if (a.Name.ToLower().StartsWith(searchValue.ToLower())) autoCompleteList.Add(a.Name);
                if (a.Name.Distance(searchValue) < 3) aprioriList.Add(a.Name);
            });

            AttributeSearch attributeSearch = new AttributeSearch(aprioriList, autoCompleteList);
            return attributeSearch;
        }
    }
}