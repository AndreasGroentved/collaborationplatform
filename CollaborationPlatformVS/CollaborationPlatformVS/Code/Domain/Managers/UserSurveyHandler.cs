﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using CollaborationPlatformVS.Code.Data;
using CollaborationPlatformVS.Code.Domain.ClientServerDataObjects;
using CollaborationPlatformVS.Code.Domain.DataObjects;
using CollaborationPlatformVS.Code.Util;
using Attribute = CollaborationPlatformVS.Code.Domain.ClientServerDataObjects.Attribute;
using Type = CollaborationPlatformVS.Code.Domain.ClientServerDataObjects.Type;

namespace CollaborationPlatformVS.Code.Domain.Managers
{
    public class UserSurveyHandler : IUserSurveyHandler
    {
        private readonly IDataManager _dataManager;
        private readonly Subject<SurveyUser> _surveySubject;

        public UserSurveyHandler(IDataManager dataManager)
        {
            _dataManager = dataManager;
            _surveySubject = new Subject<SurveyUser>();
        }

        public async Task<string> CreateSurvey(SurveySearch s)
        {
            SurveySearchResult surveySearchResult = await CreateUserSurveyRequest(s);
            await _dataManager.InsertSurvey(surveySearchResult);
            return surveySearchResult.SurveySearch.Id;
        }

        private async Task<SurveySearchResult> CreateUserSurveyRequest(SurveySearch surveySearch)
        {
            try
            {
                SurveySearchResult s = await CreateSurveySearch(surveySearch);
                s.SurveySearch.Id = Guid.NewGuid().ToString();
                return s;
            }
            catch (Exception e)
            {
                // Debug.WriteLine("Exception: " + e.Message);
            }
            return null;
        }

        public async Task<SurveySearchResult> CreateSurveySearch(SurveySearch surveySearch)
        {
            UserAttributeScore uAs = await ResultOfSurveySearch(surveySearch.Attributes);

            return new SurveySearchResult(uAs.ScorePeoples, surveySearch, uAs.AttributeCount.ToDictionary(kv => kv.Key.Name, kv => kv.Value));
        }

        private async Task<UserAttributeScore> ResultOfSurveySearch(SearchAttribute[] searchAttributes)
        {
            Dictionary<Attribute, int> attributeCount = new Dictionary<Attribute, int>();
            Dictionary<string, AttributePeople> attributePeoples = await GetUsersWithAnyAttributesOfSearch(new List<SearchAttribute>(searchAttributes));
            Dictionary<User, int> peopleScore = InitPeopleScore(attributePeoples);

            foreach (SearchAttribute search in searchAttributes)
            {
                attributeCount.Add(search.Attribute, 0);
                foreach (User user in peopleScore.Keys.ToList()) ScoreUser(user, search, attributeCount, peopleScore);
            }
            return new UserAttributeScore(GetScoreOfPeople(peopleScore), attributeCount);
        }

        private void ScoreUser(User user, SearchAttribute search, Dictionary<Attribute, int> attributeCount,
            IDictionary<User, int> peopleScore)
        {
            var temp = user.Attributes.Find(a => a.Name.Equals(search.Attribute.Name,
                StringComparison.CurrentCultureIgnoreCase));
            if (temp == null)
            {
                if (search.Not)
                {
                    attributeCount[search.Attribute] += 1;
                    peopleScore[user] += search.Must ? 5 : 1;
                }
                else peopleScore[user] -= search.Must ? 1000 : 1;
                return;
            }

            bool equal = AreAttributesEqual(search, temp);

            if (equal == !search.Not /*|| !contains == search.Not*/)
            {
                attributeCount[search.Attribute] += 1;
                peopleScore[user] += search.Must ? 5 : 1;
            }
            else peopleScore[user] -= search.Must ? 1000 : 1;
        }

        private Dictionary<User, int> InitPeopleScore(Dictionary<string, AttributePeople> attributePeoples)
        {
            Dictionary<User, int> peopleScore = new Dictionary<User, int>();
            foreach (User user in from keyValuePair in attributePeoples
                from user in keyValuePair.Value.Users
                where !peopleScore.ContainsKey(user)
                select user)
            {
                peopleScore.Add(user, 0);
            }
            return peopleScore;
        }

        private bool AreAttributesEqual(SearchAttribute search, Attribute temp)
        {
            switch (search.Attribute.Type)
            {
                case Type.INTEGER:
                case Type.FLOAT:
                    return NumberEquality(temp, search.MinVal, search.MaxVal);
                case Type.BOOLEAN:
                    return BoolEquality(temp, search.Attribute);
                case Type.STRING:
                    return StringEquality(temp, search.Attribute);
                default:
                    return false;
            }
        }

        private bool StringEquality(Attribute a, Attribute b) => a.Value.Equals(b.Value, StringComparison.OrdinalIgnoreCase);

        private bool BoolEquality(Attribute a, Attribute b) => Convert.ToBoolean(a.Value) == Convert.ToBoolean(b.Value);

        private bool NumberEquality(Attribute a, int minVal, int maxVal)
        {
            bool result = int.TryParse(a.Value, out int intVal);
            return result && MathUtil.WithInRange(minVal, maxVal, intVal);
        }

        private List<ScorePeople> GetScoreOfPeople(Dictionary<User, int> peopleScore)
        {
            List<ScorePeople> scorePeoples = (from keyValuePair in peopleScore
                where keyValuePair.Value > 0
                select new ScorePeople(keyValuePair.Key, keyValuePair.Value)).ToList();
            scorePeoples.Sort((x, y) => x.Score.CompareTo(y.Score));
            return scorePeoples;
        }

        private async Task<Dictionary<string, AttributePeople>> GetUsersWithAnyAttributesOfSearch(List<SearchAttribute> attribute)
        {
            Dictionary<string, AttributePeople> userList = new Dictionary<string, AttributePeople>();
            foreach (User u in _dataManager.GetAllUsers())
            {
                if (u.Attributes == null) continue;
                foreach (Attribute at in u.Attributes.Intersect(attribute.Select(search => search.Attribute)))
                {
                    if (userList.ContainsKey(at.Name)) userList[at.Name].Users.Add(u);
                    else userList.Add(at.Name, new AttributePeople(u, GetSearchAttributeFromList(attribute, at)));
                }
            }
            return userList;
        }

        public async Task<Dictionary<string, int>> GetSearchAttributeCount(SearchAttribute[] search)
        {
            UserAttributeScore uAs = await ResultOfSurveySearch(search);
            Dictionary<string, int> attributeCount = uAs.AttributeCount.ToDictionary(p => p.Key.Name, t => t.Value);
            attributeCount["alle"] = uAs.ScorePeoples.Sum(a => a.Score > 0 ? 1 : 0);
            return attributeCount;
        }

        private SearchAttribute GetSearchAttributeFromList(List<SearchAttribute> search, Attribute a) => search.Find(s => s.Attribute.Name == a.Name);

        public async Task<List<SurveySearch>> GetSurveys(string email)
        {
            List<SurveySearchResult> surveyList = new List<SurveySearchResult>();
            _dataManager.GetAllSurveys().ForEach(x =>
            {
                if (email.Equals(x.SurveySearch.User)) surveyList.Add(x);
                else
                {
                    x.Users.ForEach(us =>
                    {
                        if (email.Equals(us.Email)) surveyList.Add(x);
                    });
                }
            });

            List<SurveySearch> surveys = new List<SurveySearch>();
            surveyList.ForEach(s =>
            {
                SurveySearch survey = s.SurveySearch;
                survey.NumberConfirmed = 0;
                survey.NumberDenied = 0;
                survey.NumberOfUsers = s.Users.Count;
                foreach (ParticipateDTO dto in s.Users)
                {
                    if (email == dto.Email) survey.ParticipationFlag = dto.Participates;
                    if (dto.HasAnswered)
                    {
                        if (dto.Participates) survey.NumberConfirmed++;
                        else survey.NumberDenied++;
                    }
                }
                surveys.Add(survey);
            });

            return surveys;
        }

        public async Task<bool> UpdateSurvey(string email, SurveySearch s)
        {
            SurveySearchResult ss = await _dataManager.FindSurveyFromSearchId(s.Id); //TODO update   
            ParticipateDTO p = ss?.Users.Find(aa => aa.Email == email);
            if (p != null)
            {
                p.Participates = s.ParticipationFlag;
                p.HasAnswered = true;
            }

            return await _dataManager.UpdateSurvey(ss);
        }

        public IObservable<SurveyUser> GetSurveySubject() => _surveySubject;
    }
}