﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CollaborationPlatformVS.Code.Domain.ClientServerDataObjects;

namespace CollaborationPlatformVS.Code.Domain.Managers
{
    public interface IUserSurveyHandler
    {
        Task<string> CreateSurvey(SurveySearch s);
        Task<SurveySearchResult> CreateSurveySearch(SurveySearch surveySearch);
        Task<Dictionary<string, int>> GetSearchAttributeCount(SearchAttribute[] search);
        Task<List<SurveySearch>> GetSurveys(string email);
        IObservable<SurveyUser> GetSurveySubject();
        Task<bool> UpdateSurvey(string email, SurveySearch s);
    }
}