﻿using System.Threading.Tasks;
using CollaborationPlatformVS.Code.Domain.ClientServerDataObjects;

namespace CollaborationPlatformVS.Code.Domain.Managers
{
    public interface IUserManager
    {
        Task<bool> CreateUser(User user, string password);
        string GetPasswordForEncryption(string email);
        Task<User> IsLoginValid(Login login);
        Task<bool> UpdateUser(User user, string email);
    }
}