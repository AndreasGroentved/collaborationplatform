﻿using System.Threading.Tasks;
using CollaborationPlatformVS.Code.Data;
using CollaborationPlatformVS.Code.Domain.ClientServerDataObjects;
using CollaborationPlatformVS.Code.Util;

namespace CollaborationPlatformVS.Code.Domain.Managers
{
    public class UserManager : IUserManager
    {
        private readonly IDataManager _dataManager;

        public UserManager(IDataManager dataManager)
        {
            _dataManager = dataManager;
        }

        public async Task<bool> CreateUser(User user, string password)
        {
            UserDTO userDto = DomainDataConversion.ToUserDto(user);
            userDto.Password = password;
            bool created = await _dataManager.CreateUser(userDto);
            if (created) userDto.Attributes.ForEach(async a => await _dataManager.AddAttribute(a, userDto.Name));
            return created;
        }

        public async Task<User> IsLoginValid(Login login) => await _dataManager.IsLoginValid(login);
        public string GetPasswordForEncryption(string email) => _dataManager.GetPasswordForEncryption(email);
        public async Task<bool> UpdateUser(User user, string email) => await _dataManager.UpdateUser(user, email);
    }
}