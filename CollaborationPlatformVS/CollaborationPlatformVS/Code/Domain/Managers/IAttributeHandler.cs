﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CollaborationPlatformVS.Code.Domain.ClientServerDataObjects;

namespace CollaborationPlatformVS.Code.Domain.Managers
{
    public interface IAttributeHandler
    {
        Task<AttributeSearch> GetAutoCompleteAndCorrectedAttributeList(string searchValue);
        Task<List<Attribute>> GetFrequentList(List<Attribute> attributes);
    }
}