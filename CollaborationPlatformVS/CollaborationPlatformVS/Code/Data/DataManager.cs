﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MongoDB.Driver;
using CollaborationPlatformVS.Code.Util;
using System.Threading.Tasks;
using CollaborationPlatformVS.Code.Domain.ClientServerDataObjects;
using Attribute = CollaborationPlatformVS.Code.Domain.ClientServerDataObjects.Attribute;

namespace CollaborationPlatformVS.Code.Data
{
    public class DataManager : IDataManager
    {
        public const string DbName = "USERv2";
        protected static IMongoClient Client;
        protected static IMongoDatabase Database;
        private readonly IMongoDatabase _db;
        private readonly IMongoCollection<UserDTO> _userCollection;
        private readonly IMongoCollection<Attribute> _attributeCollection;
        private readonly IMongoCollection<SurveySearchResult> _surveySearchCollection;
        private readonly IDictionary<string, User> _loginDictionary;
        private readonly IDictionary<string, string> _passDictionary;

        public DataManager(string databaseName = DbName)
        {
            _db = new MongoClient().GetDatabase(databaseName); //Navnet er ikke vigtigt
            _userCollection = _db.GetCollection<UserDTO>("users");
            _userCollection.Indexes.CreateOneAsync(Builders<UserDTO>.IndexKeys.Ascending(_ => _.Email));
            _surveySearchCollection = _db.GetCollection<SurveySearchResult>("surveySearchResult");
            _attributeCollection = _db.GetCollection<Attribute>("attributes");
            _loginDictionary = new ConcurrentDictionary<string, User>();

            bool a = LoadAllUsers().Result;
            _passDictionary = new ConcurrentDictionary<string, string>();
            bool b = CreatePassDictionary().Result;
        }

        private async Task<bool> CreatePassDictionary() //TODO navngivning
        {
            await _userCollection.Find(_ => true).ForEachAsync(u =>
            {
                if (u == null || u.Email == null || u.Password == null) return;
                _passDictionary[u.Email] = u.Password;
            });
            return true;
        }

        public string GetPasswordForEncryption(string email) => _passDictionary[email];

        public bool DropTable(string tableName)
        {
            switch (tableName)
            {
                case "survey":
                case "attributes":
                case "users":
                    _db.DropCollection(tableName);
                    return true;
                default:
                    return false;
            }
        }

        private async Task<bool> LoadAllUsers()
        {
            await _userCollection.Find(_ => true).ForEachAsync(user =>
            {
                if (user.Name == null) Debug.WriteLine("null user");
                else _loginDictionary[user.Email] = DomainDataConversion.ToUser(user);
            });
            return true;
        }

        public async Task<User> IsLoginValid(Login login)
        {
            _loginDictionary.TryGetValue(login.Email, out User u);
            if (u != null) return u;
            u = await GetUser(login);
            if (u.Email.Equals("")) return u;
            _loginDictionary.Add(login.Email, u);
            return u;
        }

        public async Task<bool> CreateUser(UserDTO userDto)
        {
            var tas = await FindUser(userDto.Email);
            if (tas != null) return false;
            await InsertUser(userDto);
            _passDictionary[userDto.Email] = userDto.Password;
            _loginDictionary[userDto.Email] = DomainDataConversion.ToUser(userDto);
            return true;
        }

        public async Task<bool> AddAttribute(Attribute newAttribute, string name)
        {
            Attribute attrExists = await GetAttribute(newAttribute.Name);
            if (attrExists != null)
            {
                bool ifA = await HasAttribute(newAttribute, name);
                if (ifA) attrExists.Count++;

                await UpdateAttribute(attrExists);
                return true;
            }
            newAttribute.Count = 1;
            await InsertAttribute(newAttribute);
            return true;
        }

        public async Task<bool> UpdateAttribute(Attribute attr)
        {
            await _attributeCollection.ReplaceOneAsync(x => x.Name.Equals(attr.Name), attr);
            return true;
        }

        public async Task<bool> InsertAttribute(Attribute attr)
        {
            await _attributeCollection.InsertOneAsync(attr);
            return true;
        }


        public async Task<UserDTO> FindUser(string email)
        {
            return await (await _userCollection.FindAsync(x => x.Email.Equals(email))).FirstOrDefaultAsync();
        }

        public async Task<bool> InsertUser(UserDTO user)
        {
            await _userCollection.InsertOneAsync(user);
            return true;
        }


        public async Task<bool> HasAttribute(Attribute attribute, string name)
        {
            return await(await _userCollection.FindAsync(u => u.Name == name && u.Attributes.Contains(attribute)))
                    .AnyAsync();
        }

        private async Task<Attribute> GetAttribute(string attributeName)
        {
            //TODO nullObject
            return await (await _attributeCollection.FindAsync(a => a.Name.Equals(attributeName))).FirstOrDefaultAsync();
        }

        public async Task<List<HashSet<Attribute>>> GetAttributeSets()
        {
            List<HashSet<Attribute>> attributeListOfSets = new List<HashSet<Attribute>>();
            await (await _userCollection.FindAsync(_ => true)).ForEachAsync(user =>
            {
                if (user.Attributes != null && user.Attributes.Count > 0)
                {
                    attributeListOfSets.Add(GetUserAttributeSet(user));
                }
            });
            return attributeListOfSets;
        }

        private HashSet<Attribute> GetUserAttributeSet(UserDTO user) => new HashSet<Attribute>(user.Attributes);

        public async Task<bool> UpdateUser(User user, string email)
        {
            UserDTO userDto = await FindUser(email);
            try
            {
                user.Attributes.ForEach(async at => await AddAttribute(at, user.Name));
                userDto.Attributes = user.Attributes;
                await _userCollection.ReplaceOneAsync(x => x.Email.Equals(user.Email), userDto);
                _loginDictionary[email] = user;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: " + e.Message);
            }
            return true;
        }

        public async Task<User> GetUser(Login login)
        {
            UserDTO u = await FindUser(login.Email);
            return u == null ? User.NullUser() : DomainDataConversion.ToUser(u);
        }


        //Bemærk at argumenter ændres i stedet for returværdi, alternative kunne attributcount opdateres bagefter, og peopleScore være return værdi

        private void ReEvaluateUserForSurveys(UserDTO u) //TODO
        {
        }

        public List<User> GetAllUsers() => _loginDictionary.Values.ToList();
        public async Task<SurveySearchResult> FindSurveyFromSearchId(string searchId) => await (await _surveySearchCollection.FindAsync(x => x.SurveySearch.Id == searchId)).FirstOrDefaultAsync();


        public async Task<bool> InsertSurvey(SurveySearchResult surveySearchResult)
        {
            await _surveySearchCollection.InsertOneAsync(surveySearchResult);
            return true;
        }


        public async Task<bool> UpdateSurvey(SurveySearchResult survey)
        {
            await _surveySearchCollection.ReplaceOneAsync(x => x.Id.Equals(survey.Id), survey);
            return true;
        }

        public List<SurveySearchResult> GetAllSurveys() => _surveySearchCollection.Find(_ => true).ToList();

        public List<Attribute> GetAllAttributes() => _attributeCollection.Find(_ => true).ToList();

    }
}