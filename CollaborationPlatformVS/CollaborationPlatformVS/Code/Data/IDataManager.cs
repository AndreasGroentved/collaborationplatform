﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CollaborationPlatformVS.Code.Domain.ClientServerDataObjects;

namespace CollaborationPlatformVS.Code.Data
{
    public interface IDataManager
    {
        Task<bool> AddAttribute(Attribute newAttribute, string name);
        Task<bool> CreateUser(UserDTO userDto);
        bool DropTable(string tableName);
        Task<SurveySearchResult> FindSurveyFromSearchId(string searchId);
        Task<UserDTO> FindUser(string email);
        List<Attribute> GetAllAttributes();
        List<SurveySearchResult> GetAllSurveys();
        List<User> GetAllUsers();
        Task<List<HashSet<Attribute>>> GetAttributeSets();
        string GetPasswordForEncryption(string email);
        Task<User> GetUser(Login login);
        Task<bool> HasAttribute(Attribute attribute, string name);
        Task<bool> InsertAttribute(Attribute attr);
        Task<bool> InsertSurvey(SurveySearchResult surveySearchResult);
        Task<bool> InsertUser(UserDTO user);
        Task<User> IsLoginValid(Login login);
        Task<bool> UpdateAttribute(Attribute attr);
        Task<bool> UpdateSurvey(SurveySearchResult survey);
        Task<bool> UpdateUser(User user, string email);
    }
}