﻿using System.Collections.Generic;
using CollaborationPlatformVS.Code.Domain.ClientServerDataObjects;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace CollaborationPlatformVS.Code.Data
{
    public class UserDTO //TODO Add Gender to UserDTO
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("password")]
        public string Password { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("attributes")]
        public List<Attribute> Attributes { get; set; }

        public UserDTO()
        {
        }

        public UserDTO(List<Attribute> attributes, string email, string name)
        {
            Name = name;
            Email = email;
            Attributes = attributes;
            Password = "";
        }


        private bool Equals(UserDTO other)
        {
            return other != null && Email.Equals(other.Email);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((UserDTO) obj);
        }

        public override int GetHashCode()
        {
            return Email?.GetHashCode() ?? 0;
        }

        public static bool operator ==(UserDTO left, UserDTO right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(UserDTO left, UserDTO right)
        {
            return !Equals(left, right);
        }
    }
}