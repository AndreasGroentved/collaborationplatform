﻿using MongoDB.Bson.Serialization.Attributes;

namespace CollaborationPlatformVS.Code.Data
{
    public class ParticipateDTO
    {
        [BsonElement("participates")]
        public bool Participates { get; set; }
            
        [BsonElement("email")]
        public string Email { get; set; }
        
        [BsonElement("answered")]
        public bool HasAnswered { get; set; }
    }
}
