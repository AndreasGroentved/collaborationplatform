using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using CollaborationPlatformVS.Code.Domain;
using System.Reactive.Linq;
using System;
using System.Diagnostics;

namespace CollaborationPlatformVS.Code.Connection
{
    public class WebSocketCommunication : WebSocketHandler
    {
        private readonly ICollaborate _collaborationPlatform;

        public WebSocketCommunication(WebSocketConnectionManager webSocketConnectionManager, ICollaborate i) : base(webSocketConnectionManager)
        {
            _collaborationPlatform = i;
            _collaborationPlatform.MessageSubject.Subscribe(async socketMessage => await SendMessageAsync(socketMessage.Socketid, socketMessage.Message)); //TODO subscribeOn?
        }

        public override async Task OnConnected(WebSocket socket)
        {
            await base.OnConnected(socket);
        }

        public override async Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            var socketId = WebSocketConnectionManager.GetId(socket);
            var message = Encoding.UTF8.GetString(buffer, 0, result.Count);

            string response = await _collaborationPlatform.Receive(message, socketId);
            socketId = WebSocketConnectionManager.GetId(socket);
            if (response.Length > 0)
            {
                await SendMessageAsync(socketId, response);
            }
        }

        public override async Task OnDisconnected(WebSocket socket)
        {
            await base.OnDisconnected(socket);
        }
    }
}