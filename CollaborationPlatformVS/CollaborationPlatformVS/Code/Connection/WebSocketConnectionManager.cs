using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace CollaborationPlatformVS.Code.Connection
{
    public class WebSocketConnectionManager
    {
        private readonly ConcurrentDictionary<string, WebSocket> _sockets = new ConcurrentDictionary<string, WebSocket>();

        public WebSocket GetSocketById(string id)
        {
            return _sockets.FirstOrDefault(p => p.Key == id).Value;
        }

        public bool UserConnected(string email) => _sockets.ContainsKey(email);

        public ConcurrentDictionary<string, WebSocket> GetAll() => _sockets;


        public string GetId(WebSocket socket) => _sockets.FirstOrDefault(p => p.Value == socket).Key;

        public void AddSocket(WebSocket socket) => _sockets.TryAdd(CreateConnectionId(), socket);

        public void AddSocket(WebSocket socket, string userid) => _sockets.TryAdd(userid, socket);

        public void UpdateSocketIdToUserID(string socketid, string userid)
        {
            if (_sockets.ContainsKey(userid)) return;
            _sockets.TryRemove(socketid, out WebSocket websocket);
            _sockets.TryAdd(userid, websocket);
        }

        public async Task RemoveSocket(string id)
        {
            _sockets.TryRemove(id, out WebSocket socket);
            try
            {
                await socket.CloseAsync(closeStatus: WebSocketCloseStatus.NormalClosure,
                    statusDescription: "Closed by the WebSocketManager",
                    cancellationToken: CancellationToken.None);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: " + e.Message);
            }
        }

        private string CreateConnectionId() => Guid.NewGuid().ToString();
    }
}