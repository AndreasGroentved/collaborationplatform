webpackJsonp([1,4],{

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user_manager_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__survey_data__ = __webpack_require__(517);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserSurveyService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserSurveyService = (function () {
    function UserSurveyService(userManagerService) {
        this.userManagerService = userManagerService;
        this.surveysIn = new Array();
        this.surveysCreated = new Array();
        this.currentSurvey = new __WEBPACK_IMPORTED_MODULE_1__survey_data__["a" /* SurveyData */]();
    }
    UserSurveyService.prototype.updateSurveys = function (surveys) {
        var user = this.userManagerService.getCurrentUser();
        this.surveysCreated = [];
        this.surveysIn = [];
        for (var _i = 0, surveys_1 = surveys; _i < surveys_1.length; _i++) {
            var survey = surveys_1[_i];
            if (survey.Id == this.currentSurvey.Id) {
                this.currentSurvey = survey;
            }
            if (survey.User == user.Email) {
                this.surveysCreated.push(survey);
            }
            else {
                this.surveysIn.push(survey);
            }
        }
    };
    UserSurveyService.prototype.updateSurvey = function (updatedSurvey) {
        for (var _i = 0, _a = this.surveysIn; _i < _a.length; _i++) {
            var survey = _a[_i];
            if (survey.Id == updatedSurvey.Id) {
                survey = updatedSurvey;
                return;
            }
        }
        for (var _b = 0, _c = this.surveysCreated; _b < _c.length; _b++) {
            var survey = _c[_b];
            if (survey.Id == updatedSurvey.Id) {
                survey = updatedSurvey;
                return;
            }
        }
        //Ny survey TODO push besked --> observable, måske høre til logged in component
        if (updatedSurvey.User == this.userManagerService.getCurrentUser.name) {
            this.surveysCreated.push(updatedSurvey);
        }
        else {
            this.surveysIn.push(updatedSurvey);
        }
    };
    UserSurveyService.prototype.getSurvey = function (id) {
        this.currentSurvey.Id = id;
        for (var _i = 0, _a = this.surveysIn; _i < _a.length; _i++) {
            var survey = _a[_i];
            if (id == survey.Id) {
                return survey;
            }
        }
        for (var _b = 0, _c = this.surveysCreated; _b < _c.length; _b++) {
            var survey = _c[_b];
            if (id == survey.Id) {
                return survey;
            }
        }
        console.log("survey fail");
        return new __WEBPACK_IMPORTED_MODULE_1__survey_data__["a" /* SurveyData */]();
    };
    UserSurveyService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["c" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__user_manager_service__["a" /* UserManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__user_manager_service__["a" /* UserManagerService */]) === 'function' && _a) || Object])
    ], UserSurveyService);
    return UserSurveyService;
    var _a;
}());
//# sourceMappingURL=user-survey.service.js.map

/***/ }),

/***/ 18:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserManagerService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserManagerService = (function () {
    function UserManagerService() {
        this.mySessionStorage = sessionStorage;
        this.myLocalStorage = localStorage;
        this.initUser();
    }
    UserManagerService.prototype.initUser = function () {
        console.log("init");
        if (this.mySessionStorage.getItem('user')) {
            try {
                var userDTO = JSON.parse(this.mySessionStorage.getItem('user'));
                this.currentUser = new __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */]();
                this.currentUser.setUser(userDTO);
                this.login = JSON.parse(this.mySessionStorage.getItem('login'));
                //Check med server om login er gyldigt
                console.log("login set to ");
                console.log(this.login);
            }
            catch (e) {
                console.log(e);
            }
        }
        this.userSubject = new __WEBPACK_IMPORTED_MODULE_2_rxjs__["BehaviorSubject"](this.currentUser);
    };
    UserManagerService.prototype.getUserSubject = function () {
        return this.userSubject;
    };
    UserManagerService.prototype.getCurrentUser = function () {
        return this.currentUser;
    };
    UserManagerService.prototype.getIsValidatedLogin = function () {
        return this.isValidatedLogin;
    };
    UserManagerService.prototype.setUserFromDTO = function (userDTO) {
        var user = new __WEBPACK_IMPORTED_MODULE_1__user__["a" /* User */]();
        user.setUser(userDTO);
        this.setUser(user);
    };
    UserManagerService.prototype.setUser = function (user) {
        this.userAdded = false;
        if (user.Email === "") {
            this.currentUser = null;
        }
        else {
            this.currentUser = user;
            this.userSubject.next(this.currentUser);
        }
        this.saveUser();
    };
    UserManagerService.prototype.saveUser = function () {
        this.mySessionStorage.setItem('user', JSON.stringify(this.currentUser));
    };
    UserManagerService.prototype.getLogin = function () {
        return this.login;
    };
    UserManagerService.prototype.setLogin = function (login) {
        this.login = login;
        this.mySessionStorage.setItem('login', JSON.stringify(this.login));
    };
    UserManagerService.prototype.clearLogin = function () {
        this.login = null; //TODO måske observable til håndtering af login fejl
        this.mySessionStorage.removeItem('login');
    };
    UserManagerService.prototype.updateUser = function (attributes) {
        this.currentUser.Attributes = attributes;
    };
    UserManagerService.prototype.isLoggedIn = function () {
        return this.login != null;
    };
    UserManagerService.prototype.removeUser = function (username) {
        this.myLocalStorage.setItem('user', "");
    };
    UserManagerService.prototype.userLoggedOut = function () {
        this.currentUser = null;
        this.clearLogin();
        this.mySessionStorage.clear();
    };
    UserManagerService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(), 
        __metadata('design:paramtypes', [])
    ], UserManagerService);
    return UserManagerService;
}());
//# sourceMappingURL=user-manager.service.js.map

/***/ }),

/***/ 29:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__attribute_service__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_survey_service__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_manager_service__ = __webpack_require__(18);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SocketManagerService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SOCKET_URL = 'ws://localhost:56411/ws';
//TODO tjek user data, hver gang der kaldes metoder på serveren - eller i hvert en gang i mellem... yolo
var SocketManagerService = (function () {
    //TODO TODO surveyobservable
    //TODO suggestions broken...
    function SocketManagerService(userManagerService, userSurveyService, attributeService) {
        this.userManagerService = userManagerService;
        this.userSurveyService = userSurveyService;
        this.attributeService = attributeService;
        this.userManagerService = userManagerService;
        this.connect(SOCKET_URL);
        this.map = new Map([
            ["Login", this.login],
            ["GetUserSurveys", this.getUserSurveys],
            ["GetUserSurvey", this.getUserSurvey],
            ["AttributeSearch", this.AttributeSearch],
            ["SearchRelated", this.SearchRelated]
        ]);
        this.socketStateObserver = new __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Subject"]();
        //TODO pushing af beskeder om ny survey man deltager i
    }
    SocketManagerService.prototype.connect = function (url) {
        this.create(url);
    };
    SocketManagerService.prototype.create = function (url) {
        var _this = this;
        console.log("create");
        this.ws = new WebSocket(url);
        this.ws.onmessage = function (msg) {
            console.log(msg.data);
            try {
                var instructionClient = JSON.parse(msg.data);
                console.log(instructionClient);
            }
            catch (e) {
                return;
            }
            try {
                _this.map.get(instructionClient.MethodName)(instructionClient, _this);
            }
            catch (e) {
                console.log("undefined method: " + instructionClient.MethodName);
                console.log(e);
            }
            if (!_this.socketStateObserver.isEmpty()) {
                _this.socketStateObserver.next(true); //TODO isEmpty virker måske ikke som en bool
            } //TODO er det nu også en PACE life ide?
        };
        this.ws.onerror = function () {
            console.log("*** Socket Error ***");
        };
        this.ws.onclose = function () {
            console.log("*** Socket closing ***");
        };
        this.ws.onopen = function () {
            console.log("*** Socket open***");
            _this.socketStateObserver.next(true);
            //TODO check login - kræver noget omstruktuering, så kodekloner kan undgås - evt. klasse til at oprette forspørgsler
        };
        this.ws.close.bind(this.ws);
    };
    SocketManagerService.prototype.login = function (instruction, outer) {
        console.log("login");
        console.log(instruction.Argument);
        var user = instruction.Argument;
        if (user.Email.length == 0) {
            outer.userManagerService.clearLogin(); //TODO måske feedback
        }
        outer.userManagerService.setUserFromDTO(user); //setUserFromDTO(user);
        outer.attributeService.setAttributes(outer.userManagerService.getCurrentUser().Attributes);
    };
    SocketManagerService.prototype.SearchRelated = function (instruction, outer) {
        console.log("searchrelated");
        console.log(instruction.Argument);
        var attributes = instruction.Argument;
        outer.attributeService.setApriori(attributes);
    };
    SocketManagerService.prototype.getUserSurveys = function (instruction, outer) {
        console.log("GetSurveys");
        var surveys = instruction.Argument;
        console.log(surveys);
        outer.userSurveyService.updateSurveys(surveys);
    };
    SocketManagerService.prototype.getUserSurvey = function (instruction, outer) {
        console.log("GetSurveys");
        var survey = instruction.Argument;
        console.log(survey);
        outer.userSurveyService.updateSurvey(survey);
    };
    SocketManagerService.prototype.AttributeSearch = function (instruction, outer) {
        var attributeSearch = instruction.Argument;
        console.log(attributeSearch);
        outer.attributeService.updateSuggestions(attributeSearch);
    };
    SocketManagerService.prototype.sendRequest = function (request) {
        var _this = this;
        if (this.ws.readyState === WebSocket.OPEN) {
            this.ws.send(request);
        }
        else if (this.ws.readyState == WebSocket.CONNECTING) {
            this.socketStateObserver.first().subscribe(function () { return _this.sendRequest(request); });
            //TODO der kan forudses situationer, hvor dette kan give problemer med synkronisering, Single
            console.log("Socket not ready");
        }
        else if (this.ws.readyState == WebSocket.CLOSED) {
            this.create(SOCKET_URL);
            console.log("recreate connection");
            this.socketStateObserver.first().subscribe(function () { return _this.sendRequest(request); });
        }
        else {
        }
    };
    SocketManagerService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["c" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__user_manager_service__["a" /* UserManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__user_manager_service__["a" /* UserManagerService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__user_survey_service__["a" /* UserSurveyService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__user_survey_service__["a" /* UserSurveyService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__attribute_service__["a" /* AttributeService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__attribute_service__["a" /* AttributeService */]) === 'function' && _c) || Object])
    ], SocketManagerService);
    return SocketManagerService;
    var _a, _b, _c;
}());
//# sourceMappingURL=socket-manager.service.js.map

/***/ }),

/***/ 337:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__type__ = __webpack_require__(519);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Attribute; });

var Attribute = (function () {
    function Attribute() {
        this.Name = "def", this.Type = __WEBPACK_IMPORTED_MODULE_0__type__["a" /* Types */].STRING, this.Value = "null";
    }
    Attribute.prototype.setAttributeFromDTO = function (attrDTO) {
        this.Name = attrDTO.Name;
        this.Type = attrDTO.Type;
        this.Value = attrDTO.Value;
    };
    Attribute.prototype.setAttributeDataOld = function (Name, Value, Type) {
        if (Name === void 0) { Name = ""; }
        if (Value === void 0) { Value = null; }
        if (Type === void 0) { Type = __WEBPACK_IMPORTED_MODULE_0__type__["a" /* Types */].STRING; }
        switch (Type) {
            case __WEBPACK_IMPORTED_MODULE_0__type__["a" /* Types */].INTEGER:
            case __WEBPACK_IMPORTED_MODULE_0__type__["a" /* Types */].FLOAT:
                Value = Value;
                break;
            case __WEBPACK_IMPORTED_MODULE_0__type__["a" /* Types */].BOOLEAN:
                Value = Value;
                break;
            case __WEBPACK_IMPORTED_MODULE_0__type__["a" /* Types */].STRING:
                Value = Value;
                break;
        }
    };
    Attribute.prototype.isFloat = function (n) {
        return Number(n) === n && n % 1 !== 0;
    };
    Attribute.prototype.setAttributeData = function (Name, Value) {
        this.Value = Value;
        this.Name = Name;
        if (Value = "") {
            this.Type = __WEBPACK_IMPORTED_MODULE_0__type__["a" /* Types */].BOOLEAN;
        }
        else if (Value instanceof Number) {
            if (this.isFloat(Value)) {
                this.Type = __WEBPACK_IMPORTED_MODULE_0__type__["a" /* Types */].FLOAT;
            }
            else {
                this.Type = __WEBPACK_IMPORTED_MODULE_0__type__["a" /* Types */].INTEGER;
            }
        }
        else {
            this.Type = __WEBPACK_IMPORTED_MODULE_0__type__["a" /* Types */].STRING;
        }
    };
    return Attribute;
}());
//# sourceMappingURL=attribute.js.map

/***/ }),

/***/ 338:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_manager_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(28);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuardLoggedIn; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuardLoggedIn = (function () {
    function AuthGuardLoggedIn(userManager, router) {
        this.userManager = userManager;
        this.router = router;
    }
    AuthGuardLoggedIn.prototype.canActivate = function (route, state) {
        var match = this.router.url.replace("/\// ", "");
        if (!this.userManager.isLoggedIn()) {
            this.router.navigate(['/login']);
            return false;
        }
        if (route.url.toString() == "") {
            this.router.navigate(['/account']);
            return false;
        }
        return true;
    };
    AuthGuardLoggedIn = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__user_manager_service__["a" /* UserManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__user_manager_service__["a" /* UserManagerService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]) === 'function' && _b) || Object])
    ], AuthGuardLoggedIn);
    return AuthGuardLoggedIn;
    var _a, _b;
}());
//# sourceMappingURL=auth-guard-logged-in.service.js.map

/***/ }),

/***/ 339:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_manager_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(28);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuardLoggedOut; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuardLoggedOut = (function () {
    function AuthGuardLoggedOut(userManager, router) {
        this.userManager = userManager;
        this.router = router;
    }
    AuthGuardLoggedOut.prototype.canActivate = function (route, state) {
        if (this.userManager.isLoggedIn()) {
            // this.router.navigate([''], { queryParams: { returnUrl: state.url }}); //TODO
            return false;
        }
        return true;
    };
    AuthGuardLoggedOut = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__user_manager_service__["a" /* UserManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__user_manager_service__["a" /* UserManagerService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]) === 'function' && _b) || Object])
    ], AuthGuardLoggedOut);
    return AuthGuardLoggedOut;
    var _a, _b;
}());
//# sourceMappingURL=auth-guard-logged-out.service.js.map

/***/ }),

/***/ 340:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_manager_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__socket_manager_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(28);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoggedInComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoggedInComponent = (function () {
    function LoggedInComponent(userManager, socketManagerService, router) {
        this.userManager = userManager;
        this.socketManagerService = socketManagerService;
        this.router = router;
        console.log("Starting up loggedINComponent");
        //this.active = "account";
    }
    LoggedInComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        //TODO sideskift
        this.router.events.subscribe(function (val) {
            if (val instanceof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* NavigationEnd */]) {
                if (val.url == "/account") {
                    _this.active = "account";
                }
                else if (val.url.includes("/surveys")) {
                    console.log("surveys");
                    _this.active = "surveys";
                }
                else if (val.url.includes("/findUsers")) {
                    _this.active = "findUsers";
                }
                else {
                }
            }
        });
    };
    LoggedInComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* Component */])({
            selector: 'logged-in-component',
            template: __webpack_require__(595),
            styles: [__webpack_require__(580)],
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__user_manager_service__["a" /* UserManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__user_manager_service__["a" /* UserManagerService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__socket_manager_service__["a" /* SocketManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__socket_manager_service__["a" /* SocketManagerService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === 'function' && _c) || Object])
    ], LoggedInComponent);
    return LoggedInComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=logged-in.component.js.map

/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_manager_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__socket_manager_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__instruction_server__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login__ = __webpack_require__(513);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginComponent = (function () {
    function LoginComponent(userManager, socketManagerService, router) {
        this.userManager = userManager;
        this.socketManagerService = socketManagerService;
        this.router = router;
        this.loggedIn = false;
        console.log("Starting up loginComponent");
        this.subscribe();
    }
    LoginComponent.prototype.subscribe = function () {
        var _this = this;
        this.userManager.getUserSubject().subscribe(function (user) { _this.loggedIn = true; _this.router.navigateByUrl('/account'); }); //TODO og check om alt er godt
    };
    LoginComponent.prototype.sendLoginRequest = function (email, password) {
        var _email = email.value;
        var _password = password.value;
        var instructionServer = new __WEBPACK_IMPORTED_MODULE_4__instruction_server__["a" /* InstructionServer */]("Login", { Email: "", PassWord: "" }, {
            Email: _email,
            Password: _password
        }); //TODO få styr på serializering... yolo
        var login = new __WEBPACK_IMPORTED_MODULE_5__login__["a" /* Login */]();
        login.Email = _email, login.Password = _password;
        this.userManager.setLogin(login);
        console.log(JSON.stringify(instructionServer));
        this.socketManagerService.sendRequest(JSON.stringify(instructionServer));
    };
    LoginComponent.prototype.logoutUser = function () {
        this.userManager.userLoggedOut();
        console.log("Logged out user");
    };
    LoginComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_6" /* Component */])({
            selector: 'login-component',
            template: __webpack_require__(597),
            styles: [__webpack_require__(582)],
            encapsulation: __WEBPACK_IMPORTED_MODULE_1__angular_core__["W" /* ViewEncapsulation */].None
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__user_manager_service__["a" /* UserManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__user_manager_service__["a" /* UserManagerService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__socket_manager_service__["a" /* SocketManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__socket_manager_service__["a" /* SocketManagerService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_router__["a" /* Router */]) === 'function' && _c) || Object])
    ], LoginComponent);
    return LoginComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ 342:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__attribute_service__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user_manager_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__attribute__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__socket_manager_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__instruction_server__ = __webpack_require__(78);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SignupComponent = (function () {
    function SignupComponent(userManager, socketManagerService, attributeService, router) {
        this.userManager = userManager;
        this.socketManagerService = socketManagerService;
        this.attributeService = attributeService;
        this.router = router;
        this.attributeService.setAttributes(new Array());
    }
    SignupComponent.prototype.signup = function (name, password, repeatPassword, age, email) {
        // First check if any of the textboxes are missing input
        if (name.value === "") {
            alert("Username missing!");
            name.focus();
            return;
        }
        if (password.value === "") {
            alert("Password missing!");
            password.focus();
            return;
        }
        if (repeatPassword.value === "") {
            alert("Repeated password missing!");
            repeatPassword.focus();
            return;
        }
        if (age.value === "") {
            alert("Age missing!");
            age.focus();
            return;
        }
        if (email.value === "") {
            alert("Email missing!");
            email.focus();
            return;
        }
        // Check if passwords match
        if (password.value !== repeatPassword.value) {
            alert("Passwords do not match!");
            password.value = "";
            repeatPassword.value = ""; // Clear password fields
            return;
        }
        // Check if age is a valid number
        if (isNaN(+age.value)) {
            alert("Age needs to be a number!");
            age.value = "";
            age.focus();
            return;
        }
        // Check if email is valid
        if (!this.validateEmail(email.value)) {
            alert("Email is invalid!");
            email.value = "";
            email.focus();
            return;
        }
        var ageAttribute = new __WEBPACK_IMPORTED_MODULE_5__attribute__["a" /* Attribute */]();
        ageAttribute.setAttributeData("alder", age.value);
        this.attributeService.getAttributes().push(ageAttribute);
        var attributes = this.attributeService.getAttributes();
        var user = new __WEBPACK_IMPORTED_MODULE_4__user__["a" /* User */](name.value, email.value, attributes);
        var registrationObject = {
            User: user,
            Password: password.value
        };
        var instructionServer = new __WEBPACK_IMPORTED_MODULE_7__instruction_server__["a" /* InstructionServer */]("CreateUser", { Email: "", PassWord: "" }, registrationObject);
        console.log("Sending request: " + JSON.stringify(instructionServer));
        this.socketManagerService.sendRequest(JSON.stringify(instructionServer));
        this.router.navigateByUrl('/login');
        //this.userManager.addUser(username.value, name.value, +age.value, email.value); // + unary operator = parseInt
        // Clear textboxes
        //username.value = ""; password.value = ""; repeatPassword.value = ""; name.value = ""; age.value = ""; email.value = "";
        //TODO hold styr på svar - fx med observable
    };
    SignupComponent.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    SignupComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["_6" /* Component */])({
            template: __webpack_require__(601),
            styles: [__webpack_require__(586)],
            providers: []
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__user_manager_service__["a" /* UserManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__user_manager_service__["a" /* UserManagerService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_6__socket_manager_service__["a" /* SocketManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_6__socket_manager_service__["a" /* SocketManagerService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__attribute_service__["a" /* AttributeService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__attribute_service__["a" /* AttributeService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_router__["a" /* Router */]) === 'function' && _d) || Object])
    ], SignupComponent);
    return SignupComponent;
    var _a, _b, _c, _d;
}());
//# sourceMappingURL=signup.component.js.map

/***/ }),

/***/ 343:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = (function () {
    function User(Name, Email, Attributes) {
        if (Name === void 0) { Name = ""; }
        if (Email === void 0) { Email = ""; }
        if (Attributes === void 0) { Attributes = []; }
        this.Name = Name, this.Email = Email, this.Attributes = Attributes;
    }
    User.prototype.setUser = function (userDTO) {
        this.Name = userDTO.Name;
        this.Email = userDTO.Email;
        this.Attributes = userDTO.Attributes;
    };
    User.prototype.getName = function () {
        return this.Name;
    };
    User.prototype.getEmail = function () {
        return this.Email;
    };
    User.prototype.getInformation = function () {
        return this.Attributes;
    };
    User.prototype.editInformation = function (userInformation) {
        return false; // This is obviously not correct
    };
    return User;
}());
//# sourceMappingURL=user.js.map

/***/ }),

/***/ 384:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 384;


/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(473);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(506);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(520);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 503:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__attribute_service__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_manager_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__socket_manager_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__instruction_server__ = __webpack_require__(78);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AccountComponent = (function () {
    function AccountComponent(userManager, socketManagerService, attributeService) {
        this.userManager = userManager;
        this.socketManagerService = socketManagerService;
        this.attributeService = attributeService;
    }
    AccountComponent.prototype.updateUser = function () {
        this.userManager.updateUser(this.attributeService.getAttributes());
        this.userManager.saveUser();
        var attributes = this.attributeService.getAttributes();
        var instructionServer = new __WEBPACK_IMPORTED_MODULE_4__instruction_server__["a" /* InstructionServer */]("UpdateUser", this.userManager.getLogin(), this.userManager.getCurrentUser());
        console.log("Sending request: " + JSON.stringify(instructionServer));
        this.socketManagerService.sendRequest(JSON.stringify(instructionServer));
    };
    AccountComponent.prototype.ngAfterViewInit = function () {
    };
    AccountComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_6" /* Component */])({
            template: __webpack_require__(589),
            styles: [__webpack_require__(574)],
            selector: 'account-component',
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__user_manager_service__["a" /* UserManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__user_manager_service__["a" /* UserManagerService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__socket_manager_service__["a" /* SocketManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__socket_manager_service__["a" /* SocketManagerService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__attribute_service__["a" /* AttributeService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__attribute_service__["a" /* AttributeService */]) === 'function' && _c) || Object])
    ], AccountComponent);
    return AccountComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=account.component.js.map

/***/ }),

/***/ 504:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login_component__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__signup_signup_component__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__logged_in_logged_in_component__ = __webpack_require__(340);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__auth_guard_logged_in_service__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__auth_guard_logged_out_service__ = __webpack_require__(339);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var appRoutes = [
    //{ path: 'account', component: MainContentComponent, outlet: 'content', canActivate: [AuthGuardLoggedIn] },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_2__login_login_component__["a" /* LoginComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_6__auth_guard_logged_out_service__["a" /* AuthGuardLoggedOut */]] },
    { path: 'register', component: __WEBPACK_IMPORTED_MODULE_3__signup_signup_component__["a" /* SignupComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_6__auth_guard_logged_out_service__["a" /* AuthGuardLoggedOut */]] },
    { path: '', component: __WEBPACK_IMPORTED_MODULE_4__logged_in_logged_in_component__["a" /* LoggedInComponent */], pathMatch: 'full', canActivate: [__WEBPACK_IMPORTED_MODULE_5__auth_guard_logged_in_service__["a" /* AuthGuardLoggedIn */]] },
    { path: '**', component: __WEBPACK_IMPORTED_MODULE_4__logged_in_logged_in_component__["a" /* LoggedInComponent */], pathMatch: 'full', canActivate: [__WEBPACK_IMPORTED_MODULE_5__auth_guard_logged_in_service__["a" /* AuthGuardLoggedIn */]] },
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */].forRoot(appRoutes)
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */]
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], AppRoutingModule);
    return AppRoutingModule;
}());
//# sourceMappingURL=app-routing.module.js.map

/***/ }),

/***/ 505:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_manager_service__ = __webpack_require__(18);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppComponent = (function () {
    function AppComponent(userManager, router) {
        this.userManager = userManager;
        this.router = router;
        this.title = '(Konge) Collaboration Platform';
        router.events.subscribe(function (val) {
            console.log(val);
        });
    }
    AppComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__(590),
            styles: [__webpack_require__(575)],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* ViewEncapsulation */].None
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__user_manager_service__["a" /* UserManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__user_manager_service__["a" /* UserManagerService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === 'function' && _b) || Object])
    ], AppComponent);
    return AppComponent;
    var _a, _b;
}());
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 506:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__survey_survey_component__ = __webpack_require__(518);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__create_survey_create_survey_component__ = __webpack_require__(509);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__account_account_component__ = __webpack_require__(503);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__attributes_attributes_component__ = __webpack_require__(507);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(469);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__core_core_module__ = __webpack_require__(508);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_routing_app_routing_module__ = __webpack_require__(504);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__(505);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__login_login_component__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__home_home_component__ = __webpack_require__(510);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__signup_signup_component__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__navbar_navbar_component__ = __webpack_require__(515);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__page_not_found_page_not_found_component__ = __webpack_require__(516);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__logged_out_logged_out_component__ = __webpack_require__(512);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__logged_in_logged_in_component__ = __webpack_require__(340);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__left_menu_left_menu_component__ = __webpack_require__(511);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__main_content_main_content_component__ = __webpack_require__(514);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




















var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_5__angular_core__["b" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_11__login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_12__home_home_component__["a" /* HomeComponent */],
                __WEBPACK_IMPORTED_MODULE_13__signup_signup_component__["a" /* SignupComponent */],
                __WEBPACK_IMPORTED_MODULE_14__navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_15__page_not_found_page_not_found_component__["a" /* PageNotFoundComponent */],
                __WEBPACK_IMPORTED_MODULE_16__logged_out_logged_out_component__["a" /* LoggedOutComponent */],
                __WEBPACK_IMPORTED_MODULE_17__logged_in_logged_in_component__["a" /* LoggedInComponent */],
                __WEBPACK_IMPORTED_MODULE_18__left_menu_left_menu_component__["a" /* LeftMenuComponent */],
                __WEBPACK_IMPORTED_MODULE_19__main_content_main_content_component__["a" /* MainContentComponent */],
                __WEBPACK_IMPORTED_MODULE_3__attributes_attributes_component__["a" /* AttributeComponent */],
                __WEBPACK_IMPORTED_MODULE_2__account_account_component__["a" /* AccountComponent */],
                __WEBPACK_IMPORTED_MODULE_1__create_survey_create_survey_component__["a" /* CreateSurveyComponent */],
                __WEBPACK_IMPORTED_MODULE_0__survey_survey_component__["a" /* SurveyComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_9__app_routing_app_routing_module__["a" /* AppRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_8__core_core_module__["a" /* CoreModule */] // Contains our singleton services
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */]]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 507:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__attribute_service__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__attribute__ = __webpack_require__(337);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_manager_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__socket_manager_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__instruction_server__ = __webpack_require__(78);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AttributeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var AttributeComponent = (function () {
    //TODO gøre så der også kan vises uden tilføjelse af attributer
    //TODO kommer nok problemer med kloner, steder hvor der er attributer
    function AttributeComponent(userManager, socketManagerService, router, attributeService) {
        this.userManager = userManager;
        this.socketManagerService = socketManagerService;
        this.router = router;
        this.attributeService = attributeService;
        //this.attributeService.setAttributes(new Array());
        if (this.router.url == "/account") {
            console.log("happened");
            this.attributeService.setAttributes(this.userManager.getCurrentUser().Attributes); //TODO temp
            this.attributeService.setApriori(new Array); //TODO probs?
        }
    }
    AttributeComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.routerSub = this.router.events.subscribe(function (val) {
            if (!_this.userManager.isLoggedIn()) {
                _this.routerSub.unsubscribe();
            }
            if (val.url == "/account") {
                _this.attributeService.setAttributes(_this.userManager.getCurrentUser().Attributes); //TODO måske opdatere med serveren? PACE out
            }
        });
        //this.getSuggestions();
    };
    AttributeComponent.prototype.onkey = function (event) {
        console.log("key up");
        if (event.target.value.length > 2) {
            this.getAttributeSuggestions(event.target.value);
        }
        else {
            this.attributeService.setSuggestions(new Array());
        }
        console.log(event.target.value.length);
    };
    AttributeComponent.prototype.getAttributeSuggestions = function (searchVal) {
        var instructionServer = new __WEBPACK_IMPORTED_MODULE_6__instruction_server__["a" /* InstructionServer */]("AttributeSearch", this.userManager.getLogin(), searchVal);
        console.log(JSON.stringify(instructionServer));
        this.socketManagerService.sendRequest(JSON.stringify(instructionServer));
    };
    AttributeComponent.prototype.select = function (item) {
        //this.query = item;
        //this.suggestions = [];
    };
    AttributeComponent.prototype.ngOnDestroy = function () {
        this.attributeService.setSuggestions(new Array());
    };
    AttributeComponent.prototype.addAttribute = function (name, value) {
        var tempAttr = new __WEBPACK_IMPORTED_MODULE_2__attribute__["a" /* Attribute */]();
        tempAttr.setAttributeData(name.value, value.value);
        this.attributeService.getAttributes().push(tempAttr);
        // this.attributeService.setAttributes(this.attributes);
        this.getSuggestions();
    };
    AttributeComponent.prototype.getSuggestions = function () {
        var instructionServer = new __WEBPACK_IMPORTED_MODULE_6__instruction_server__["a" /* InstructionServer */]("SearchRelated", { Email: "", PassWord: "" }, this.attributeService.getAttributes());
        console.log(JSON.stringify(instructionServer));
        this.socketManagerService.sendRequest(JSON.stringify(instructionServer));
    };
    AttributeComponent.prototype.deleteAttribute = function (index) {
        this.attributeService.getAttributes().splice(index, 1);
    };
    AttributeComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["_6" /* Component */])({
            template: __webpack_require__(591),
            styles: [__webpack_require__(576)],
            selector: 'attributes-component',
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__user_manager_service__["a" /* UserManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__user_manager_service__["a" /* UserManagerService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__socket_manager_service__["a" /* SocketManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__socket_manager_service__["a" /* SocketManagerService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__attribute_service__["a" /* AttributeService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__attribute_service__["a" /* AttributeService */]) === 'function' && _d) || Object])
    ], AttributeComponent);
    return AttributeComponent;
    var _a, _b, _c, _d;
}());
//# sourceMappingURL=attributes.component.js.map

/***/ }),

/***/ 508:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user_survey_service__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__attribute_service__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__socket_manager_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__user_manager_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__auth_guard_logged_in_service__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__auth_guard_logged_out_service__ = __webpack_require__(339);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CoreModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var CoreModule = (function () {
    function CoreModule() {
    }
    CoreModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["b" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__angular_common__["a" /* CommonModule */]
            ],
            declarations: [],
            providers: [__WEBPACK_IMPORTED_MODULE_4__socket_manager_service__["a" /* SocketManagerService */], __WEBPACK_IMPORTED_MODULE_5__user_manager_service__["a" /* UserManagerService */], __WEBPACK_IMPORTED_MODULE_6__auth_guard_logged_in_service__["a" /* AuthGuardLoggedIn */], __WEBPACK_IMPORTED_MODULE_7__auth_guard_logged_out_service__["a" /* AuthGuardLoggedOut */], __WEBPACK_IMPORTED_MODULE_1__attribute_service__["a" /* AttributeService */], __WEBPACK_IMPORTED_MODULE_0__user_survey_service__["a" /* UserSurveyService */]] // Will provide these services as singletons
        }), 
        __metadata('design:paramtypes', [])
    ], CoreModule);
    return CoreModule;
}());
//# sourceMappingURL=core.module.js.map

/***/ }),

/***/ 509:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user_survey_service__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__attribute_service__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user_manager_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__socket_manager_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__instruction_server__ = __webpack_require__(78);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateSurveyComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CreateSurveyComponent = (function () {
    function CreateSurveyComponent(userManager, socketManagerService, attributeService, userSurveyService) {
        this.userManager = userManager;
        this.socketManagerService = socketManagerService;
        this.attributeService = attributeService;
        this.userSurveyService = userSurveyService;
        this.attributeService.setAttributes(new Array());
    }
    CreateSurveyComponent.prototype.createUserSurvey = function (fra, til, antal, beskrivelse) {
        var login = this.userManager.getLogin();
        var instructionServer = new __WEBPACK_IMPORTED_MODULE_5__instruction_server__["a" /* InstructionServer */]("CreateUserSurvey", this.userManager.getCurrentUser(), {
            Id: "-1",
            Attributes: this.attributeService.getAttributes(),
            User: login.Email,
            FromDate: new Date(),
            ToDate: new Date(),
            NumberOfUsers: antal.value,
            CreationDate: new Date(),
            ParticipationFlag: true,
            NumberConfirmed: 0,
            NumberDenied: 0,
            Description: beskrivelse.value
        }); //TODO få styr på serializering... yolo
        console.log(JSON.stringify(instructionServer));
        this.socketManagerService.sendRequest(JSON.stringify(instructionServer));
    };
    CreateSurveyComponent.prototype.ngAfterViewInit = function () {
    };
    CreateSurveyComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["_6" /* Component */])({
            template: __webpack_require__(592),
            styles: [__webpack_require__(577)],
            selector: 'create-survey-component',
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__user_manager_service__["a" /* UserManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__user_manager_service__["a" /* UserManagerService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__socket_manager_service__["a" /* SocketManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__socket_manager_service__["a" /* SocketManagerService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__attribute_service__["a" /* AttributeService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__attribute_service__["a" /* AttributeService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__user_survey_service__["a" /* UserSurveyService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__user_survey_service__["a" /* UserSurveyService */]) === 'function' && _d) || Object])
    ], CreateSurveyComponent);
    return CreateSurveyComponent;
    var _a, _b, _c, _d;
}());
//# sourceMappingURL=create-survey.component.js.map

/***/ }),

/***/ 510:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = (function () {
    function HomeComponent() {
    }
    HomeComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* Component */])({
            template: __webpack_require__(593),
            styles: [__webpack_require__(578)]
        }), 
        __metadata('design:paramtypes', [])
    ], HomeComponent);
    return HomeComponent;
}());
//# sourceMappingURL=home.component.js.map

/***/ }),

/***/ 511:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_manager_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__socket_manager_service__ = __webpack_require__(29);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeftMenuComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LeftMenuComponent = (function () {
    function LeftMenuComponent(userManager, socketManagerService, router) {
        this.userManager = userManager;
        this.socketManagerService = socketManagerService;
        this.router = router;
    }
    LeftMenuComponent.prototype.logOut = function () {
        this.userManager.userLoggedOut();
        this.router.navigateByUrl('/');
    };
    LeftMenuComponent.prototype.ngAfterViewInit = function () {
        console.log(this.el);
        //this.el.nativeElement.click();
        //this.def.nativeElement.click();
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_7" /* ViewChild */])('varName'), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* ElementRef */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* ElementRef */]) === 'function' && _a) || Object)
    ], LeftMenuComponent.prototype, "el", void 0);
    LeftMenuComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_6" /* Component */])({
            template: __webpack_require__(594),
            styles: [__webpack_require__(579)],
            selector: 'left-menu-component',
        }), 
        __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__user_manager_service__["a" /* UserManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__user_manager_service__["a" /* UserManagerService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__socket_manager_service__["a" /* SocketManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__socket_manager_service__["a" /* SocketManagerService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_router__["a" /* Router */]) === 'function' && _d) || Object])
    ], LeftMenuComponent);
    return LeftMenuComponent;
    var _a, _b, _c, _d;
}());
//# sourceMappingURL=left-menu.component.js.map

/***/ }),

/***/ 512:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_manager_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__socket_manager_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(28);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoggedOutComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoggedOutComponent = (function () {
    function LoggedOutComponent(userManager, socketManagerService, router) {
        this.userManager = userManager;
        this.socketManagerService = socketManagerService;
        console.log("Starting up loggedOutComponent");
        router.events.subscribe(function (val) {
            console.log("yolo   " + val.url);
        });
    }
    LoggedOutComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* Component */])({
            selector: 'logged-out-component',
            template: __webpack_require__(596),
            styles: [__webpack_require__(581)],
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__user_manager_service__["a" /* UserManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__user_manager_service__["a" /* UserManagerService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__socket_manager_service__["a" /* SocketManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__socket_manager_service__["a" /* SocketManagerService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === 'function' && _c) || Object])
    ], LoggedOutComponent);
    return LoggedOutComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=logged-out.component.js.map

/***/ }),

/***/ 513:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Login; });
var Login = (function () {
    function Login() {
    }
    Login.prototype.Login = function () {
        this.Email = "";
        this.Password = "";
    };
    return Login;
}());
//# sourceMappingURL=login.js.map

/***/ }),

/***/ 514:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__user_manager_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__socket_manager_service__ = __webpack_require__(29);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainContentComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MainContentComponent = (function () {
    function MainContentComponent(userManager, socketManagerService) {
        this.userManager = userManager;
        this.socketManagerService = socketManagerService;
    }
    MainContentComponent.prototype.ngAfterViewInit = function () {
    };
    MainContentComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* Component */])({
            template: __webpack_require__(598),
            styles: [__webpack_require__(583)],
            selector: 'main-content-component',
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* ViewEncapsulation */].None
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__user_manager_service__["a" /* UserManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__user_manager_service__["a" /* UserManagerService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__socket_manager_service__["a" /* SocketManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__socket_manager_service__["a" /* SocketManagerService */]) === 'function' && _b) || Object])
    ], MainContentComponent);
    return MainContentComponent;
    var _a, _b;
}());
//# sourceMappingURL=main-content.component.js.map

/***/ }),

/***/ 515:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavbarComponent = (function () {
    function NavbarComponent() {
    }
    NavbarComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* Component */])({
            selector: 'navbar',
            template: __webpack_require__(599),
            styles: [__webpack_require__(584)],
            encapsulation: __WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* ViewEncapsulation */].None
        }), 
        __metadata('design:paramtypes', [])
    ], NavbarComponent);
    return NavbarComponent;
}());
//# sourceMappingURL=navbar.component.js.map

/***/ }),

/***/ 516:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PageNotFoundComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PageNotFoundComponent = (function () {
    function PageNotFoundComponent() {
    }
    PageNotFoundComponent.prototype.ngOnInit = function () {
    };
    PageNotFoundComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* Component */])({
            template: __webpack_require__(600),
            styles: [__webpack_require__(585)]
        }), 
        __metadata('design:paramtypes', [])
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());
//# sourceMappingURL=page-not-found.component.js.map

/***/ }),

/***/ 517:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurveyData; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SurveyData = (function () {
    function SurveyData() {
        this.Attributes = new Array();
        this.FromDate = new Date(); //TODO håndtering af datorer...
        this.ToDate = new Date();
        this.ToDate.setMinutes(this.FromDate.getMinutes() + 60);
        this.NumberOfUsers = 20;
        this.ParticipationFlag = false;
        this.Id = "-1";
        this.Description = "invalid survey";
    }
    SurveyData.prototype.setData = function (description, attributes, user, fromDate, toDate, numberOfUsers, participationFlag, numberConfirmed, numberDenied, id) {
        this.Attributes = attributes;
        this.User = user;
        this.FromDate = fromDate;
        this.ToDate = toDate;
        this.NumberOfUsers = numberOfUsers;
        this.ParticipationFlag = participationFlag;
        this.NumberConfirmed = numberConfirmed;
        this.NumberDenied = numberDenied;
        this.Id = id;
        this.Description = description;
    };
    SurveyData = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(), 
        __metadata('design:paramtypes', [])
    ], SurveyData);
    return SurveyData;
}());
//# sourceMappingURL=survey-data.js.map

/***/ }),

/***/ 518:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__attribute_service__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__user_survey_service__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_manager_service__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__socket_manager_service__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__instruction_server__ = __webpack_require__(78);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurveyComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SurveyComponent = (function () {
    function SurveyComponent(userManager, socketManagerService, userSurveyService, router, attributeService) {
        var _this = this;
        this.userManager = userManager;
        this.socketManagerService = socketManagerService;
        this.userSurveyService = userSurveyService;
        this.router = router;
        this.attributeService = attributeService;
        this.router.events.subscribe(function (val) {
            _this.handleRouterUrl(val.url);
        });
        this.handleRouterUrl(router.url);
        var login = this.userManager.getLogin();
        var instructionServer = new __WEBPACK_IMPORTED_MODULE_6__instruction_server__["a" /* InstructionServer */]("GetUserSurveys", login, {}); //TODO få styr på serializering... yolo
        console.log(JSON.stringify(instructionServer));
        this.socketManagerService.sendRequest(JSON.stringify(instructionServer));
    }
    SurveyComponent.prototype.handleRouterUrl = function (url) {
        if (url == "/surveys") {
            this.showSingle = false;
        }
        else if (url.includes("/surveys/")) {
            this.showSingle = true;
            var id = url.split("/").pop();
            this.userSurveyService.currentSurvey = this.userSurveyService.getSurvey(id);
            if (this.currentSurvey) {
                this.attributeService.setAttributes(this.currentSurvey.Attributes);
            }
            console.log(this.currentSurvey);
        }
    };
    SurveyComponent.prototype.goToSurvey = function (id) {
    };
    SurveyComponent.prototype.ngAfterViewInit = function () {
    };
    SurveyComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["_6" /* Component */])({
            template: __webpack_require__(602),
            styles: [__webpack_require__(587)],
            selector: 'survey-component',
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__user_manager_service__["a" /* UserManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__user_manager_service__["a" /* UserManagerService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__socket_manager_service__["a" /* SocketManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__socket_manager_service__["a" /* SocketManagerService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__user_survey_service__["a" /* UserSurveyService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__user_survey_service__["a" /* UserSurveyService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__attribute_service__["a" /* AttributeService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__attribute_service__["a" /* AttributeService */]) === 'function' && _e) || Object])
    ], SurveyComponent);
    return SurveyComponent;
    var _a, _b, _c, _d, _e;
}());
//# sourceMappingURL=survey.component.js.map

/***/ }),

/***/ 519:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Types; });
var Types;
(function (Types) {
    Types[Types["INTEGER"] = 0] = "INTEGER";
    Types[Types["FLOAT"] = 1] = "FLOAT";
    Types[Types["BOOLEAN"] = 2] = "BOOLEAN";
    Types[Types["STRING"] = 3] = "STRING";
})(Types || (Types = {}));
//# sourceMappingURL=type.js.map

/***/ }),

/***/ 520:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 574:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(19)();
// imports


// module
exports.push([module.i, "\r\n:host{\r\n     float:left;\r\n    /* width: 33%;*/     \r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 575:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(19)();
// imports


// module
exports.push([module.i, ":host{\r\n    margin: auto;\r\n}\r\n.title{\r\n    background-color: #C7E8AC;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 576:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(19)();
// imports


// module
exports.push([module.i, ":host{    \r\n    width: 33%;     \r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 577:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(19)();
// imports


// module
exports.push([module.i, "\r\n:host{\r\n     float:left;\r\n    /* width: 33%;*/     \r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 578:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(19)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 579:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(19)();
// imports


// module
exports.push([module.i, ":host {\r\n    float: left;\r\n    width: 15%;\r\n    min-height: 400px;\r\n}\r\n.nav-sidebar { \r\n    width: 100%;\r\n    padding: 8px 0; \r\n    border-right: 1px solid #ddd;\r\n}\r\n.nav-sidebar a {\r\n    color: #333;\r\n    -webkit-transition: all 0.08s linear;\r\n    transition: all 0.08s linear; \r\n    border-radius: 4px 0 0 4px; \r\n}\r\n.nav-sidebar .active a { \r\n    cursor: default;\r\n    background-color: #428bca; \r\n    color: #fff; \r\n    text-shadow: 1px 1px 1px #666; \r\n}\r\n.nav-sidebar .active a:hover {\r\n    background-color: #428bca;   \r\n}\r\n.nav-sidebar .text-overflow a,\r\n.nav-sidebar .text-overflow .media-body {\r\n    white-space: nowrap;\r\n    overflow: hidden;\r\n    text-overflow: ellipsis; \r\n}\r\n\r\n/* Right-aligned sidebar */\r\n.nav-sidebar.pull-right { \r\n    border-right: 0; \r\n    border-left: 1px solid #ddd; \r\n}\r\n.nav-sidebar.pull-right a { \r\n    border-radius: 0 4px 4px 0; \r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 580:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(19)();
// imports


// module
exports.push([module.i, "/*#content {\r\n    float:left;\r\n}\r\n:host {\r\n    width: 33%;         \r\n    margin: auto;\r\n}*/", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 581:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(19)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 582:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(19)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 583:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(19)();
// imports


// module
exports.push([module.i, "\r\n:host{\r\n     float:left;\r\n    /* width: 33%;*/     \r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 584:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(19)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 585:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(19)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 586:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(19)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 587:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(19)();
// imports


// module
exports.push([module.i, "\r\n:host{\r\n    float:left;\r\n    width: 80%;\r\n}\r\n.survey {\r\n    text-align: center;\r\n}\r\n.attribute {\r\n    background-color: #D5E8D4;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 589:
/***/ (function(module, exports) {

module.exports = "<div>\r\n    {{userManager.currentUser.Name}} {{userManager.currentUser.Email}}\r\n</div>\r\n\r\n<attributes-component></attributes-component>\r\n\r\n<input type=\"button\" (click)=\"updateUser()\" value=\"Opdater\" class=\"btn btn-default\" />"

/***/ }),

/***/ 590:
/***/ (function(module, exports) {

module.exports = "<h1 class=\"title\">\r\n  {{title}}\r\n</h1>\r\n\r\n<router-outlet></router-outlet>"

/***/ }),

/***/ 591:
/***/ (function(module, exports) {

module.exports = "<div class=\"input-group\">\r\n    <span class=\"input-group-addon\">Navn</span>\r\n    <input type=\"text\" id=\"attributNavn\" (keyup)=\"onkey($event)\" #attributNavn class=\"form-control\" placeholder=\"Navn på attribut\"\r\n    />\r\n</div>\r\n<div class=\"input-group\">\r\n    <span class=\"input-group-addon\">Værdi</span>\r\n    <input type=\"text\" id=\"attributVal\" #attributVal class=\"form-control\" placeholder=\"Værdi af attribut\" />\r\n</div>\r\n\r\n\r\n<div *ngIf=\"attributeService.suggestions.length > 0\">\r\n    <ul>\r\n        <li *ngFor=\"let item of attributeService.suggestions\">\r\n            <a (click)=\"select(item)\">{{item}}</a>\r\n        </li>\r\n    </ul>\r\n</div>\r\n\r\n<input type=\"button\" (click)=\"addAttribute(attributNavn, attributVal)\" value=\"Tilføj\" class=\"btn btn-default\" />\r\n<p>Attributes</p>\r\n<ul>\r\n    <li *ngFor=\"let attr of attributeService.attributes; let i = index\">\r\n        {{ attr.Name }} {{attr.Value}} <span (click)=\"deleteAttribute(i)\"> X</span>\r\n    </li>\r\n</ul>\r\n\r\n<div *ngIf=\"attributeService.apriori.length > 0\">\r\n    <span><b>Suggestions</b></span>\r\n    <ul>\r\n        <li *ngFor=\"let item of attributeService.apriori\">\r\n            <a (click)=\"select(item)\">{{item.Name}}</a>\r\n        </li>\r\n    </ul>\r\n</div>"

/***/ }),

/***/ 592:
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <form>\r\n        <div class=\"input-group\">\r\n\t        <span class=\"input-group-addon\">Fra:</span>\r\n\t        <input type=\"text\" id=\"fra\" #fra class=\"form-control\" placeholder=\"Fra\" />\r\n        </div>\r\n        <div class=\"input-group\">\r\n\t        <span class=\"input-group-addon\">Til:</span>\r\n\t        <input type=\"text\" id=\"til\" #til class=\"form-control\" placeholder=\"Til\" />\r\n        </div>\r\n        <div class=\"input-group\">\r\n\t        <span class=\"input-group-addon\">Antal:</span>\r\n\t        <input type=\"number\" id=\"antal\" #antal class=\"form-control\" placeholder=\"Antal\" />\r\n        </div>\r\n        <div class=\"input-group\">\r\n\t        <span class=\"input-group-addon\">Beskrivelse:</span>\r\n\t        <input type=\"text\" id=\"beskrivelse\" #beskrivelse class=\"form-control\" placeholder=\"Beskrivelse\" />\r\n        </div>\r\n\r\n        <attributes-component></attributes-component>\r\n\r\n        <input type=\"button\" (click)=\"createUserSurvey(fra, til,antal,beskrivelse)\" value=\"Opret\" class=\"btn btn-default\" />\r\n    </form>\r\n</div>"

/***/ }),

/***/ 593:
/***/ (function(module, exports) {

module.exports = "home works!"

/***/ }),

/***/ 594:
/***/ (function(module, exports) {

module.exports = " <div class=\"row\">\r\n    <div class=\"col-md-10\">\r\n        <nav class=\"nav-sidebar\">\r\n            <ul class=\"nav\">\r\n                <div #userArea>\r\n                    <li>\r\n                        <a #varName id=\"default\" routerLink=\"/account\" routerLinkActive=\"active\">Rediger bruger</a>\r\n                    </li>\r\n                    <li>\r\n                        <a (click)=\"logOut()\" routerLink=\"/\" routerLinkActive=\"active\">Log ud</a>\r\n                        <!-- style=\"cursor: pointer;\" -->\r\n                    </li>\r\n                </div>\r\n                <div #navigationArea>\r\n                    <li>\r\n                        <a routerLink=\"/findUsers\" routerLinkActive=\"active\">Find brugere</a>\r\n                    </li>\r\n                    <li>\r\n                        <a routerLink=\"/findExpert\" routerLinkActive=\"active\">Find ekspert</a>\r\n                    </li>\r\n                    <li>\r\n                        <a routerLink=\"/findPartner\" routerLinkActive=\"active\">Find samarbejdspartner</a>\r\n                    </li>\r\n                    <li>\r\n                        <a routerLink=\"/surveys\" routerLinkActive=\"active\">Se status på ansøgninger</a>\r\n                    </li>\r\n                </div>\r\n            </ul>\r\n        </nav>\r\n    </div>\r\n </div>"

/***/ }),

/***/ 595:
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n  <left-menu-component class=\"col-md-4\"></left-menu-component>\r\n  <div [ngSwitch]=\"active\" class=\"col-md-8\">\r\n  <!-- TODO måske få routing til at virke her, men ikke prioritet-->\r\n    <template [ngSwitchCase]=\"'account'\">\r\n      <account-component></account-component>\r\n    </template>\r\n    <template [ngSwitchCase]=\"'surveys'\">\r\n      <survey-component></survey-component>\r\n    </template>\r\n    <template [ngSwitchCase]=\"'findUsers'\">\r\n      <create-survey-component></create-survey-component>\r\n    </template>\r\n    <template ngSwitchDefault=\"account\">\r\n      ikke boom\r\n    </template>\r\n  </div>\r\n</div>"

/***/ }),

/***/ 596:
/***/ (function(module, exports) {

module.exports = "<router-outlet name=\"logged-out\"></router-outlet>\r\n"

/***/ }),

/***/ 597:
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <form>\r\n    <div class=\"input-group\">\r\n      <span class=\"input-group-addon\">Email:</span>\r\n      <input type=\"text\" id=\"txtEmail\" #txtEmail class=\"form-control\" placeholder=\"Email\" />\r\n    </div>\r\n    <div class=\"input-group\">\r\n      <span class=\"input-group-addon\">Password:</span>\r\n      <input type=\"password\" id=\"txtPassword\" #txtPassword class=\"form-control\" placeholder=\"Password\" />\r\n    </div>\r\n    <!--TODO enter knap -->\r\n    <input type=\"button\" class=\"btn btn-default\" (click)=\"sendLoginRequest(txtEmail, txtPassword)\" value=\"Login\" />\r\n  </form>\r\n\r\n  <!-- TODO undersøg om dette bryder med mvvm pattern -->\r\n  <a routerLink=\"/register\" routerLinkActive=\"active\">Register</a>\r\n</div>"

/***/ }),

/***/ 598:
/***/ (function(module, exports) {

module.exports = "woop woop"

/***/ }),

/***/ 599:
/***/ (function(module, exports) {

module.exports = "<input type=\"button\" routerLink=\"/home\" value=\"Home\" class=\"btn btn-default\" />\r\n<input type=\"button\" routerLink=\"/login\" value=\"Login\" class=\"btn btn-default\" />\r\n<input type=\"button\" routerLink=\"/signup\" value=\"Signup\" class=\"btn btn-default\" />\r\n<hr /><br />"

/***/ }),

/***/ 600:
/***/ (function(module, exports) {

module.exports = "404..or something.. page not found."

/***/ }),

/***/ 601:
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <form>\r\n        <div class=\"input-group\">\r\n            <span class=\"input-group-addon\">Name:</span>\r\n            <input type=\"text\" #Name class=\"form-control\" placeholder=\"Name\" />\r\n        </div>\r\n        <div class=\"input-group\">\r\n            <span class=\"input-group-addon\">Password:</span>\r\n            <input type=\"password\" #txtPassword class=\"form-control\" placeholder=\"Password\" />\r\n        </div>\r\n        <div class=\"input-group\">\r\n            <span class=\"input-group-addon\">Repeat Password:</span>\r\n            <input type=\"password\" #txtRepeatPassword class=\"form-control\" placeholder=\"Repeat Password\" />\r\n        </div>\r\n        <div class=\"input-group\">\r\n            <span class=\"input-group-addon\">Age:</span>\r\n            <input type=\"number\" #txtAge class=\"form-control\" placeholder=\"Age\" />\r\n        </div>\r\n        <div class=\"input-group\">\r\n            <span class=\"input-group-addon\">Email:</span>\r\n            <input type=\"text\" #txtEmail class=\"form-control\" placeholder=\"Email\" />\r\n        </div>\r\n\r\n        <attributes-component></attributes-component>\r\n\r\n        <!-- TODO enter button -->\r\n        <input type=\"button\" value=\"Create Account\" (click)=\"signup(Name, txtPassword, txtRepeatPassword, txtAge, txtEmail)\" class=\"btn btn-default\"\r\n        />\r\n    </form>\r\n</div>"

/***/ }),

/***/ 602:
/***/ (function(module, exports) {

module.exports = "<div class=\"survey\">\r\n          <div *ngIf=\"showSingle\">\r\n\r\n                    <!--TODO hvis ingen undersøgelser-->\r\n                    <h1>Brugerundersøgelse</h1>\r\n\r\n                    {{userSurveyService.currentSurvey.NumberConfirmed+userSurveyService.currentSurvey.NumberDenied}} af {{userSurveyService.currentSurvey.NumberOfUsers}}\r\n                    <br/> Svartid tilbage: 13 dage\r\n                    <!--TODO fix hardcode-->\r\n                    <br/> {{userSurveyService.currentSurvey.NumberConfirmed}} kan deltage\r\n\r\n                    <ul>\r\n                              <li *ngFor=\"let attr of userSurveyService.currentSurvey.Attributes; let i = index\">\r\n                                        {{ attr.Name }} {{attr.Value}}\r\n                                        <!--TODO orden attribute-component, så det kan bruges her i stedet for -->\r\n                              </li>\r\n                    </ul>\r\n\r\n          </div>\r\n\r\n          <div *ngIf=\"!showSingle\">\r\n                    <p>Oprettede brugerundersøgelser</p>\r\n                    <ul>\r\n                              <li *ngFor=\"let survey of userSurveyService.surveysCreated; let i = index\">\r\n                                        Brugerundersøgelse: {{ survey.Id }} <input type=\"button\" routerLink=\"/surveys/{{survey.Id}}\"\r\n                                                  value=\"{{survey.Id}}\" />\r\n                              </li>\r\n                    </ul>\r\n          </div>\r\n</div>"

/***/ }),

/***/ 66:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AttributeService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AttributeService = (function () {
    function AttributeService() {
        this.attributes = new Array();
        this.suggestions = new Array();
    }
    AttributeService.prototype.updateSuggestions = function (attributeSearch) {
        this.suggestions = attributeSearch.CorrectionList.concat(attributeSearch.AutoCompleteList);
        this.suggestions = Array.from(new Set(this.suggestions));
    };
    AttributeService.prototype.getApriori = function () {
        return this.apriori;
    };
    AttributeService.prototype.setApriori = function (attributes) {
        this.apriori = attributes;
    };
    AttributeService.prototype.setAttributes = function (attributes) {
        console.log("attributter");
        console.log(attributes[0]);
        this.attributes = attributes;
    };
    AttributeService.prototype.getAttributes = function () {
        return this.attributes;
    };
    AttributeService.prototype.setSuggestions = function (suggestions) {
        this.suggestions = suggestions;
    };
    AttributeService.prototype.getSuggestions = function () {
        return this.suggestions;
    };
    AttributeService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(), 
        __metadata('design:paramtypes', [])
    ], AttributeService);
    return AttributeService;
}());
//# sourceMappingURL=attribute.service.js.map

/***/ }),

/***/ 78:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InstructionServer; });
var InstructionServer = (function () {
    function InstructionServer(MethodName, login, Argument) {
        this.MethodName = MethodName;
        this.login = login;
        this.Argument = Argument;
    }
    return InstructionServer;
}());
//# sourceMappingURL=instruction-server.js.map

/***/ }),

/***/ 869:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(385);


/***/ })

},[869]);
//# sourceMappingURL=main.bundle.js.map