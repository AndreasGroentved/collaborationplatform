﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using CollaborationPlatformVS.Code.Domain.Algorithms;
using Xunit;

namespace CollaborationPlatformVS.Tests
{
    public class LevenTest : IDisposable
    {
        private readonly Random _random = new Random();

        public LevenTest()
        {
        }

        public void Dispose()
        {
            //  throw new NotImplementedException();
        }

        [Fact]
        public void TestLevenshteinTwo()
        {
            var distance = "est".Distance("fest");
            Assert.Equal(1, distance);
            distance = "st".Distance("fest");
            Assert.Equal(2, distance);
            distance = "hest".Distance("fest");
            Assert.Equal(1, distance);
            distance = "bost".Distance("fest");
            Assert.Equal(2, distance);
            distance = "boom".Distance("ikke");
            Assert.Equal(3, distance);
            distance = "t".Distance("fest");
            Assert.Equal(3, distance);
            distance = "a".Distance("fest");
            Assert.Equal(3, distance);
            distance = "fest".Distance("t");
            Assert.Equal(3, distance);
            distance = "fest".Distance("a");
            Assert.Equal(3, distance);
            distance = "a".Distance("a");
            Assert.Equal(0, distance);
            distance = "b".Distance("a");
            Assert.Equal(1, distance);
            distance = "".Distance("");
            Assert.Equal(0, distance);
            distance = "abc".Distance("");
            Assert.Equal(3, distance);
            distance = "".Distance("abc");
            Assert.Equal(3, distance);
        }

        [Fact]
        public void SpeedLevenshteinTwo()
        {
            var watch = Stopwatch.StartNew();

            List<string> wordListA = new List<string>(1000);
            List<string> wordListB = new List<string>(1000);

            for (int i = 0; i < 1000; i++)
            {
                wordListA.Add(RandomString(9));
                wordListB.Add(RandomString(9));
            }

            for (int i = 0; i < 1000; i++)
            {
                Debug.WriteLine("");
                watch.Restart();
                int dis1 = wordListA[i].Distance(wordListB[i], 10);
                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                Debug.Write("Andreas " + elapsedMs);
                watch.Restart();
                int dis2 = wordListA[i].LevenshteinDistance(wordListB[i]);
                watch.Stop();
                elapsedMs = watch.ElapsedMilliseconds;
                Debug.Write("Leven " + elapsedMs);
                Debug.Write(" distance: " + dis1 + ", " + dis2);
                Debug.Write("word a " + wordListA[i] + ", word b " + wordListB[i]);
            }

            Assert.Equal(true, true);
        }

        public string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                    .Select(s => s[_random.Next(s.Length)]).ToArray());
        }
    }
}