using System;
using System.Collections.Generic;
using CollaborationPlatformVS.Code.Data;
using CollaborationPlatformVS.Code.Domain.ClientServerDataObjects;
using CollaborationPlatformVS.Code.Domain.Managers;
using CollaborationPlatformVS.Code.Util;
using Xunit;
using Attribute = CollaborationPlatformVS.Code.Domain.ClientServerDataObjects.Attribute;
using Type = CollaborationPlatformVS.Code.Domain.ClientServerDataObjects.Type;

namespace CollaborationPlatformVS.Tests
{
    public class DataManagerTests : IDisposable
    {
        private readonly DataManager _dm;
        private readonly UserManager _userManager;
        private readonly UserSurveyHandler _userSurveyHandler;
        private readonly AttributeHandler _attributeHandler;
        private readonly User _testUser;
        private readonly User _testUser2;
        private readonly User _testUser3;
        private readonly Login _loginObject;
        private readonly string _userName; //TODO kig p� om dette ikke er for meget tilstand
        private readonly List<Attribute> _attributes;
        private readonly List<Attribute> _attributes2;
        private readonly List<Attribute> _attributes3;

        // Constructor is run first - initialize what is needed for the tests
        public DataManagerTests()
        {
            // Initialize DataManager with test database
            _dm = new DataManager("TestDataBase");
            _userManager = new UserManager(_dm);
            _userSurveyHandler = new UserSurveyHandler(_dm);
            _attributeHandler = new AttributeHandler(_dm);

            // Initialize variables specific to the general test user
            _userName = "Random Name";
            var email = "bogus@email.com";
            var password = "password";

            // Create Login object for testing
            _loginObject = new Login(email, password);

            // Create attributes
            _attributes = new List<Attribute> // To avoid compiler thinking it's System.Attribute
            {
                new Attribute
                {
                    Name = "FavoriteColor",
                    Value = "Pink",
                    Type = Type.STRING,
                    Count = 1
                },
                new Attribute
                {
                    Name = "FavoriteFood",
                    Value = "Fermented Fish",
                    Type = Type.STRING,
                    Count = 1
                },
                new Attribute
                {
                    Name = "FavoriteNumber",
                    Value = "7",
                    Type = Type.INTEGER,
                    Count = 1
                },
                new Attribute
                {
                    Name = "Football",
                    Value = "true",
                    Type = Type.BOOLEAN,
                    Count = 1
                },
                new Attribute
                {
                    Name = "Programming",
                    Value = "true",
                    Type = Type.BOOLEAN,
                    Count = 1
                }
            };

            // Initialize user for general testing with above specifics
            _testUser = new User
            {
                Name = _userName,
                Email = email,
                Gender = "Male",
                Attributes = _attributes
            };

            // Create good and bad attributes for testing survey search
            _attributes2 = new List<Attribute> {new Attribute {Name = "FavoriteColor", Value = "Blue", Type = Type.STRING, Count = 1}};
            _attributes3 = new List<Attribute> {new Attribute {Name = "FavoriteColor", Value = "Black", Type = Type.STRING, Count = 1}};

            // Create good and bad users for testing survey search
            _testUser2 = new User {Name = "The Right Name", Email = "right@email.com", Gender = "Male", Attributes = _attributes2};
            _testUser3 = new User {Name = "Not The Right Name", Email = "wrong@email.com", Gender = "Unknown", Attributes = _attributes3};


            // Add users to database (make sure this async method runs synchronously)
            _userManager.CreateUser(_testUser, password).Wait();
            _userManager.CreateUser(_testUser2, "goodpassword").Wait();
            _userManager.CreateUser(_testUser3, "badpassword").Wait();
        }

        [Fact]
        public void TestHasAttribute()
        {
            // test if the user has specific attributes
            Attribute att1 = new Attribute
            {
                Name = "FavoriteColor",
                Value = "Pink",
                Type = Type.STRING,
                Count = 1
            };
            Attribute att2 = new Attribute
            {
                Name = "FavoriteFood",
                Value = "Pizza",
                Type = Type.STRING,
                Count = 1
            };

            Assert.True(_dm.HasAttribute(att1, _userName).Result);
            Assert.False(_dm.HasAttribute(att2, _userName).Result);
        }

        [Fact]
        public void TestGetUser()
        {
            Assert.Equal(_testUser, _dm.GetUser(_loginObject).Result);
        }

        [Fact]
        public void TestUpdateUser()
        {
            // Edit user information
            List<Attribute> attributes = new List<Attribute>
            {
                new Attribute
                {
                    Name = "Soccer",
                    Value = "true",
                    Type = Type.BOOLEAN,
                    Count = 1
                }
            };
            _testUser.EditInformation(attributes, _userName);

            // Check if changes were correct (old list was replaced)
            Assert.Equal(_testUser.Attributes, attributes);
        }

        [Fact]
        public void TestWithInRange()
        {
            Assert.True(MathUtil.WithInRange(-3, -2, -2.07));
            Assert.True(MathUtil.WithInRange(0, 22222, 30));
            Assert.True(MathUtil.WithInRange(1, 3, 2.07));
            Assert.True(MathUtil.WithInRange(10, 20, 10.001));
            Assert.True(MathUtil.WithInRange(20, 10, 19.999));
            Assert.True(MathUtil.WithInRange(1, 1, 1));
            Assert.False(MathUtil.WithInRange(1, 1, 1.0000000001));
        }

        [Fact]
        public void TestCreateSurveySearchGeneralTestUser()
        {
            // Create the SurveySearch based on the SearchAttribute array based on the
            // attributes of the general test user (the one we want to find)
            SearchAttribute[] searchAttributes = CreateSearchAttributes(_attributes, true, false);
            SurveySearch ss = CreateSurveySearch(searchAttributes);

            // Create the search result and retrieve the eligible participants
            SurveySearchResult ssr = _userSurveyHandler.CreateSurveySearch(ss).Result;
            List<ParticipateDTO> participants = ssr.Users;

            // Make assertions
            Assert.Equal(1, participants.Count);
            Assert.Equal(_testUser.Email, participants[0].Email);
        }

        [Fact]
        public void TestCreateSurveySearchUser2()
        {
            // Create the SurveySearch based on the SearchAttribute array based on the
            // attributes of testUser2 (the one we want to find)
            SearchAttribute[] searchAttributes = CreateSearchAttributes(_attributes2, true, false);
            SurveySearch ss = CreateSurveySearch(searchAttributes);

            // Create the search result and retrieve the eligible participants
            SurveySearchResult ssr = _userSurveyHandler.CreateSurveySearch(ss).Result;
            List<ParticipateDTO> participants = ssr.Users;

            // Make assertions
            Assert.Equal(1, participants.Count);
            Assert.Equal(_testUser2.Email, participants[0].Email);
        }

        [Fact]
        public void TestCreateSurveySearchUser3()
        {
            // Create the SurveySearch based on the SearchAttribute array based on the
            // attributes of testUser3 (the one we want to find)
            SearchAttribute[] searchAttributes = CreateSearchAttributes(_attributes3, true, false);
            SurveySearch ss = CreateSurveySearch(searchAttributes);

            // Create the search result and retrieve the eligible participants
            SurveySearchResult ssr = _userSurveyHandler.CreateSurveySearch(ss).Result;
            List<ParticipateDTO> participants = ssr.Users;

            // Make assertions
            Assert.Equal(1, participants.Count);
            Assert.Equal(_testUser3.Email, participants[0].Email);
        }

        [Fact]
        public void TestCreateSurveySearchNotTestUser()
        {
            // Create the SurveySearch based on the SearchAttribute array based on the
            // attributes of the general test user (we want to find the other two)
            SearchAttribute[] searchAttributes = CreateSearchAttributes(_attributes, true, true);
            SurveySearch ss = CreateSurveySearch(searchAttributes);

            // Create the search result and retrieve the eligible participants
            SurveySearchResult ssr = _userSurveyHandler.CreateSurveySearch(ss).Result;
            List<ParticipateDTO> participants = ssr.Users;

            // Make assertions
            Assert.Equal(2, participants.Count);
            string firstEmail = participants[0].Email;
            string secondEmail = participants[1].Email;
            Assert.True((firstEmail.Equals(_testUser2.Email) || secondEmail.Equals(_testUser2.Email)) &&
                        (firstEmail.Equals(_testUser3.Email) || secondEmail.Equals(_testUser3.Email)));
        }

        [Fact]
        public void TestCreateSurveySearchNotUser2()
        {
            // Create the SurveySearch based on the SearchAttribute array based on the
            // attributes of testUser2 (we want to find the other two)
            SearchAttribute[] searchAttributes = CreateSearchAttributes(_attributes2, true, true);
            SurveySearch ss = CreateSurveySearch(searchAttributes);

            // Create the search result and retrieve the eligible participants
            SurveySearchResult ssr = _userSurveyHandler.CreateSurveySearch(ss).Result;
            List<ParticipateDTO> participants = ssr.Users;

            // Make assertions
            Assert.Equal(2, participants.Count);
            string firstEmail = participants[0].Email;
            string secondEmail = participants[1].Email;
            Assert.True((firstEmail.Equals(_testUser.Email) || secondEmail.Equals(_testUser.Email)) &&
                        (firstEmail.Equals(_testUser3.Email) || secondEmail.Equals(_testUser3.Email)));
        }

        [Fact]
        public void TestCreateSurveySearchNotUser3()
        {
            // Create the SurveySearch based on the SearchAttribute array based on the
            // attributes of testUser3 (we want to find the other two)
            SearchAttribute[] searchAttributes = CreateSearchAttributes(_attributes3, true, true);
            SurveySearch ss = CreateSurveySearch(searchAttributes);

            // Create the search result and retrieve the eligible participants
            SurveySearchResult ssr = _userSurveyHandler.CreateSurveySearch(ss).Result;
            List<ParticipateDTO> participants = ssr.Users;

            // Make assertions
            Assert.Equal(2, participants.Count);
            string firstEmail = participants[0].Email;
            string secondEmail = participants[1].Email;
            Assert.True((firstEmail.Equals(_testUser.Email) || secondEmail.Equals(_testUser.Email)) &&
                        (firstEmail.Equals(_testUser2.Email) || secondEmail.Equals(_testUser2.Email)));
        }


        public void Dispose()
        {
            // Drop tables after tests
            _dm.DropTable("survey");
            _dm.DropTable("attributes");
            _dm.DropTable("users");
        }

        #region private methods for making the test methods cleaner

        private SearchAttribute[] CreateSearchAttributes(List<Attribute> attributes, bool must, bool not)
        {
            SearchAttribute[] saArr = new SearchAttribute[attributes.Count];
            for (int i = 0; i < saArr.Length; i++)
            {
                saArr[i] = new SearchAttribute
                {
                    Attribute = attributes[i],
                    Frequency = 7, // Doesn't matter
                    MinVal = -1, // minimum, if int (we don't know what Attribute is at this point)
                    MaxVal = Int32.MaxValue, // maximum, if int (we don't know what Attribute is at this point)
                    Must = must, // This flag indicates whether or not the attribute is a requirement
                    Not = not // This flag indicates whether or not the attribute should be absent
                };
            }
            return saArr;
        }

        private SurveySearch CreateSurveySearch(SearchAttribute[] searchAttributes, int numberOfUsers = 100)
        {
            return new SurveySearch
            {
                Attributes = searchAttributes,
                Description = "Ting", // Doesn't matter
                FromDate = new DateTime(), // Doesn't matter
                ToDate = new DateTime(), // Doesn't matter
                Id = "Ting", // Doesn't matter
                NumberConfirmed = 0, // Doesn't matter
                NumberDenied = 0, // Doesn't matter
                NumberOfUsers = numberOfUsers, // Max number of users to get in the search result
                User = "does@not.matter" // Email of the user creating the search
            };
        }

        #endregion
    }
}