﻿using System;
using System.Collections.Generic;
using CollaborationPlatformVS.Code.Data;
using CollaborationPlatformVS.Code.Domain.Managers;
using Xunit;
using Attribute = CollaborationPlatformVS.Code.Domain.ClientServerDataObjects.Attribute;
using Type = CollaborationPlatformVS.Code.Domain.ClientServerDataObjects.Type;

namespace CollaborationPlatformVS.Tests
{
    public class AprioriTest : IDisposable //TODO meget ufærdig...
    {
        private readonly DataManager _dataManager;
        private readonly AttributeHandler _attributeHandler;

        public AprioriTest()
        {
            _attributeHandler = new AttributeHandler(_dataManager);
        }

        public void Dispose()
        {
            //  throw new NotImplementedException();
        }


        [Fact]
        public async void Apriori()
        {
            List<Attribute> attributes = new List<Attribute>
            {
                new Attribute()
                {
                    Name = "PACE",
                    Value = "PACE",
                    Count = 1,
                    Type = Type.STRING
                }
            };
            await _attributeHandler.GetFrequentList(attributes);
        }
    }
}